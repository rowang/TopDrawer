      SUBROUTINE UGMESH(OPTN,LSUB,ARAY,MDIM,NDIM,TRNS,WKAR,LDIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                MESH SURFACE GENERATION SUBROUTINE                 *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO GENERATE A DESCRIPTION OF A MESH  *
C *  SURFACE.  A USER SUPPLIED SUBROUTINE IS CALLED TO PROCESS THE    *
C *  LINE SEGMENT END POINTS.                                         *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGMESH(OPTN,LSUB,ARAY,MDIM,NDIM,TRNS,WKAR,LDIM)           *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    ARAY  THE ARRAY DEFINING THE MESH SURFACE.                     *
C *    MDIM  THE EXTENT OF THE MESH SURFACE IN THE X DIRECTION.       *
C *    NDIM  THE EXTENT OF THE MESH SURFACE IN THE Y DIRECTION.       *
C *    TRNS  THE PROJECTION TRANSFORMATION.                           *
C *    WKAR  A WORK AREA.                                             *
C *    LDIM  THE NUMBER OF WORDS IN WKAR.                             *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      EXTERNAL      LSUB
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      REAL          TRNS(31)
      REAL          WKAR(*)
      INTEGER       LDIM
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(19)
      INTEGER*4     EXST(3),EXLO,EXNB
      REAL*4        EXTL
      EQUIVALENCE   (EXLO,EXST(1)),      (EXNB,EXST(2)),
     X              (EXTL,EXST(3))
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
      INTEGER*4     HFCU,HFFL
C
      REAL          PNTX(2)
      REAL          PNT1(2),PNT2(2),PNT3(2),PNT4(2),PNT5(2),PNT6(2)
      INTEGER       VSWT,V1IX,V1LO,V1HI,V1DL,V1CD,V2IX,V2LO,V2HI,V2DL
      REAL          ULVL
      LOGICAL       VFLG
C
      REAL          FLT1,FLT2,FLT3,FLT4,FLT5
      INTEGER       INT1,INT2
C
      DATA          INST/3,1,5,1,1,4HLOWE,4HR   ,
     X                     1,6,2,1,4HNOCO,4HMN  ,
     X                     3,5,3,0,4HTOLE,4HR   /
C
C
C  SCAN THE OPTIONS LIST.
      EXLO=0
      EXNB=0
      EXTL=0.00005
      CALL UGOPTN(OPTN,INST,EXST)
      PNTX(1)=1E10
      PNTX(2)=1E10
      IF (EXLO.EQ.0) THEN
        ULVL=1.0
      ELSE
        ULVL=-1.0
      END IF
      IF ((MDIM.LT.3).OR.(NDIM.LT.3)) GO TO 701
C
C  INITIALIZE THE HEIGHT FUNCTION.
      INT1=3*(MIN(LDIM,32767)/3)-2
      IF (INT1.LT.7) GO TO 702
      HFP1(1)=4
      HFP1(2)=0
      WKAR(1)=HFB1
      WKAR(2)=-1E10
      WKAR(3)=-1E10*ULVL
      HFP1(1)=0
      HFP1(2)=1
      WKAR(4)=HFB1
      WKAR(5)=1E10
      WKAR(6)=-1E10*ULVL
      DO 101 INT2=7,INT1,3
        IF (INT2.EQ.INT1) THEN
          HFP1(1)=0
        ELSE
          HFP1(1)=INT2+3
        END IF
        WKAR(INT2)=HFB1
  101 CONTINUE
      HFCU=1
      HFFL=7
C
C  COMPUTE THE NORMALIZED VIEW OF THE MESH SURFACE.
      PNT1(1)=TRNS(13)+TRNS(25)*TRNS(16)
      PNT1(2)=TRNS(14)+TRNS(25)*TRNS(17)
      FLT1=ARAY(1,NDIM)-ARAY(1,2)
      FLT2=(PNT1(1)-ARAY(1,2))*(ARAY(MDIM,1)-ARAY(2,1))
      FLT3=(PNT1(2)-ARAY(2,1))*FLT1-FLT2
      FLT4=(ARAY(1,2)+ARAY(1,NDIM))/2.0
      FLT5=(ARAY(2,1)+ARAY(MDIM,1))/2.0
      V1CD=0
      IF ((PNT1(2)-ARAY(MDIM,1))*FLT1+FLT2.LT.0.0) THEN
        IF (FLT3.GT.0.0) THEN
          VSWT=1
          V2LO=2
          V2HI=NDIM
          V2DL=1
          IF (PNT1(2).GT.FLT5) GO TO 202
          GO TO 201
        ELSE
          VSWT=0
          V2LO=2
          V2HI=MDIM
          V2DL=1
          IF (PNT1(1).LT.FLT4) GO TO 205
          GO TO 204
        END IF
      ELSE
        IF (FLT3.LT.0.0) THEN
          VSWT=1
          V2LO=NDIM
          V2HI=2
          V2DL=-1
          IF (PNT1(2).LT.FLT5) GO TO 201
          GO TO 202
        ELSE
          VSWT=0
          V2LO=MDIM
          V2HI=2
          V2DL=-1
          IF (PNT1(1).GT.FLT4) GO TO 204
          GO TO 205
        END IF
      END IF
  201 V1LO=MDIM
      V1HI=2
      V1DL=-1
      GO TO 203
  202 V1LO=2
      V1HI=MDIM
      V1DL=1
  203 IF (PNT1(2).GE.ARAY(MDIM,1)) V1CD=MDIM
      IF (PNT1(2).LE.ARAY(2,1)) V1CD=2
      GO TO 301
  204 V1LO=2
      V1HI=NDIM
      V1DL=1
      GO TO 206
  205 V1LO=NDIM
      V1HI=2
      V1DL=-1
  206 IF (PNT1(1).GE.ARAY(1,NDIM)) V1CD=NDIM
      IF (PNT1(1).LE.ARAY(1,2)) V1CD=2
C
C  INITIALIZE THE LOOP ON THE V2IX VARIABLE.
  301 V2IX=V2LO
C
C  MARCH ALONG THE V2IX=CONSTANT MESH LINE.
  401 V1IX=V1LO
      CALL UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PNT2)
      PNT1(1)=PNT2(1)
      PNT1(2)=PNT2(2)
  402 V1IX=V1IX+V1DL
      CALL UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PNT2)
      CALL UGMES3(PNT1,PNT2,ULVL,EXTL,WKAR,HFCU,PNT3,PNT4,VFLG)
      IF (VFLG) THEN
        IF ((V2IX.NE.V2LO).OR.(EXNB.EQ.0))
     X     CALL UGMES2(LSUB,PNT3,PNT4,PNTX)
        CALL UGMES4(PNT3,PNT4,EXTL,WKAR,HFCU,HFFL,INT1)
        IF (INT1.NE.0) GO TO 702
      END IF
      IF (V1IX.NE.V1HI) THEN
        PNT1(1)=PNT2(1)
        PNT1(2)=PNT2(2)
        GO TO 402
      END IF
C
C  CHECK FOR THE LAST V2IX=CONSTANT MESH LINE.
      IF (V2IX.EQ.V2HI) GO TO 601
C
C  MARCH BACK ALONG THE V2IX=CONSTANT MESH LINE.
      V1IX=V1HI
  501 CALL UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PNT2)
      PNT1(1)=PNT2(1)
      PNT1(2)=PNT2(2)
      V2IX=V2IX+V2DL
      CALL UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PNT2)
      V2IX=V2IX-V2DL
      CALL UGMES3(PNT1,PNT2,ULVL,EXTL,WKAR,HFCU,PNT3,PNT4,VFLG)
      IF (V1IX.EQ.V1CD) THEN
        PNT5(1)=PNT3(1)
        PNT5(2)=PNT3(2)
        PNT6(1)=PNT4(1)
        PNT6(2)=PNT4(2)
        PNT3(1)=PNT1(1)
        PNT3(2)=PNT1(2)
        PNT4(1)=PNT2(1)
        PNT4(2)=PNT2(2)
      END IF
      IF (((V1IX.EQ.V1CD).AND.(EXNB.EQ.0)).OR.(VFLG.AND.
     X  ((V1IX.NE.V1CD).OR.(EXNB.EQ.0))))
     X   CALL UGMES2(LSUB,PNT3,PNT4,PNTX)
      IF (V1IX.EQ.V1CD) THEN
        PNT3(1)=PNT5(1)
        PNT3(2)=PNT5(2)
        PNT4(1)=PNT6(1)
        PNT4(2)=PNT6(2)
      END IF
      IF (VFLG) THEN
        CALL UGMES4(PNT3,PNT4,EXTL,WKAR,HFCU,HFFL,INT1)
        IF (INT1.NE.0) GO TO 702
      END IF
      IF (V1IX.NE.V1LO) THEN
        V1IX=V1IX-V1DL
        GO TO 501
      END IF
C
C  INCREMENT IN THE V2IX DIRECTION.
      V2IX=V2IX+V2DL
      GO TO 401
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
  601 UGELV=0
      UGENM='        '
      UGEIX=0
  602 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  701 CALL UGRERR(3,'UGMESH  ',1)
      GO TO 602
  702 CALL UGRERR(3,'UGMESH  ',2)
      GO TO 602
C
      END
      SUBROUTINE UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PPNT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *              OBTAIN A PROJECTED POINT ON THE SURFACE              *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGMESH TO EVALUATE A POINT ON THE     *
C *  MESH SURFACE AND PROJECT IT INTO THE VIEW PLANE.                 *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGMES1(VSWT,V1IX,V2IX,ARAY,MDIM,NDIM,TRNS,PPNT)           *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    VSWT  AN X/Y VARIABLE SWITCH.                                  *
C *    V1IX  THE INDEX IN THE NORMALIZED X DIRECTION.                 *
C *    V2IX  THE INDEX IN THE NORMALIZED Y DIRECTION.                 *
C *    ARAY  THE ARRAY DEFINING THE MESH SURFACE.                     *
C *    MDIM  THE EXTENT OF THE MESH SURFACE IN THE X DIRECTION.       *
C *    NDIM  THE EXTENT OF THE MESH SURFACE IN THE Y DIRECTION.       *
C *    TRNS  THE PROJECTION TRANSFORMATION.                           *
C *    PPNT  THE PROJECTED POINT.                                     *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       VSWT,V1IX,V2IX
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      REAL          TRNS(31)
      REAL          PPNT(2)
C
      REAL          PT3D(3)
C
C  OBTAIN THE POINT AND PROJECT IT INTO THE VIEW PLANE.
      IF (VSWT.EQ.0) THEN
        PT3D(1)=ARAY(1,V1IX)
        PT3D(2)=ARAY(V2IX,1)
        PT3D(3)=ARAY(V2IX,V1IX)
      ELSE
        PT3D(1)=ARAY(1,V2IX)
        PT3D(2)=ARAY(V1IX,1)
        PT3D(3)=ARAY(V1IX,V2IX)
      END IF
      CALL UGPROJ(TRNS,PT3D,PPNT)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGMES2(LSUB,PNT1,PNT2,PNTX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                        DRAW A LINE SEGMENT                        *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGMESH TO DRAW A SINGLE LINE          *
C *  SEGMENT.  REDUNDANT BLANK MOVEMENTS ARE SUPPRESSED.              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGMES2(LSUB,PNT1,PNT2,PNTX)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    PNT1  THE FIRST POINT.                                         *
C *    PNT2  THE SECOND POINT.                                        *
C *    PNTX  A POINT USED TO REMEMBER THE LAST DRAWN POINT.           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      EXTERNAL      LSUB
      REAL          PNT1(2),PNT2(2),PNTX(2)
C
C  DRAW THE VECTOR AND SAVE CURRENT POSITION.
      IF ((PNT1(1).NE.PNTX(1)).OR.(PNT1(2).NE.PNTX(2)))
     X  CALL LSUB(PNT1(1),PNT1(2),0)
      CALL LSUB(PNT2(1),PNT2(2),1)
      PNTX(1)=PNT2(1)
      PNTX(2)=PNT2(2)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGMES3(PNT1,PNT2,ULVL,TOLR,WKAR,HFCU,PNT3,PNT4,VFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             CHECK A LINE AGAINST THE HEIGHT FUNCTION              *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGMESH TO CHECK THE CURRENT LINE      *
C *  SEGMENT AGAINST THE HEIGHT FUNCTION.  IF PART OF THE LINE IS     *
C *  VISIBLE, A FLAG IS SET AND THE VISIBLE PART IS SAVED IN TWO      *
C *  OUTPUT POINTS.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGMES3(PNT1,PNT2,ULVL,TOLR,WKAR,HFCU,PNT3,PNT4,VFLG)      *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PNT1  AN END POINT OF THE CURRENT LINE SEGMENT.                *
C *    PNT2  AN END POINT OF THE CURRENT LINE SEGMENT.                *
C *    ULVL  A VALUE OF 1.0 FOR AN UPPER SURFACE AND -1.0 FOR A       *
C *          LOWER SURFACE.                                           *
C *    TOLR  A TOLERANCE.                                             *
C *    WKAR  A WORK AREA.                                             *
C *    HFCU  THE INDEX OF THE CURRENT HEIGHT FUNCTION ELEMENT.        *
C *    PNT3  AN END POINT OF THE VISIBLE LINE SEGMENT.                *
C *    PNT4  AN END POINT OF THE VISIBLE LINE SEGMENT.                *
C *    VFLG  A FLAG INDICATING IF A VISIBLE SEGMENT IS AVAILABLE.     *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PNT1(2),PNT2(2)
      REAL          ULVL
      REAL*4        TOLR
      REAL          WKAR(*)
      INTEGER*4     HFCU
      REAL          PNT3(2),PNT4(2)
      LOGICAL       VFLG
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
C
      REAL          XINP(4),YINP(4)
      INTEGER       VIND(2)
C
      REAL          FLT1,FLT2,FLT3,FLT4,FLT5,FLT6,FLT7,FLT8,FLT9
      INTEGER       INT1,INT2,INT3
C
C  COMPARE LINE SEGMENT WITH HEIGHT FUNCTION.
      VFLG=.TRUE.
      PNT3(1)=PNT1(1)
      PNT3(2)=PNT1(2)
      PNT4(1)=PNT2(1)
      PNT4(2)=PNT2(2)
      FLT1=TOLR*(PNT2(1)-PNT1(1))
      FLT2=TOLR*(PNT2(2)-PNT1(2))
      XINP(1)=PNT1(1)+FLT1
      YINP(1)=PNT1(2)+FLT2
      XINP(2)=PNT2(1)-FLT1
      YINP(2)=PNT2(2)-FLT2
      DO 108 INT1=1,2
  101   IF (WKAR(HFCU+1).GE.XINP(INT1)) THEN
          HFB1=WKAR(HFCU)
          HFCU=HFP1(2)
          GO TO 101
        END IF
        INT2=0
  102   HFB1=WKAR(HFCU)
        INT3=HFP1(1)
        FLT1=XINP(INT1)-WKAR(INT3+1)
        IF (FLT1.LE.0.0) GO TO 104
  103   HFCU=INT3
        GO TO 102
  104   IF (FLT1.EQ.0.0) THEN
          INT2=1
          IF (ULVL*(YINP(INT1)-WKAR(INT3+2)).GE.0.0) GO TO 103
          GO TO 106
        END IF
        IF (INT2.NE.0) GO TO 105
        IF (ULVL*(((WKAR(HFCU+2)-WKAR(INT3+2))/(WKAR(HFCU+1)-
     X    WKAR(INT3+1)))*FLT1+WKAR(INT3+2)-YINP(INT1)).GT.0.0)
     X    GO TO 106
  105   VIND(INT1)=1
        GO TO 107
  106   VIND(INT1)=0
  107   CONTINUE
  108 CONTINUE
      IF ((VIND(1).EQ.0).AND.(VIND(2).EQ.0)) GO TO 302
      IF ((VIND(1).EQ.1).AND.(VIND(2).EQ.1)) GO TO 301
C
C  INTERSECT THE HEIGHT FUNCTION AND THE LINE SEGMENT.
  201 IF (WKAR(HFCU+1).GE.MIN(PNT3(1),PNT4(1))) THEN
        HFB1=WKAR(HFCU)
        HFCU=HFP1(2)
        GO TO 201
      END IF
  202 HFB1=WKAR(HFCU)
      INT3=HFP1(1)
      IF (INT3.EQ.0) GO TO 302
      FLT1=WKAR(HFCU+1)-WKAR(INT3+1)
      FLT2=WKAR(HFCU+2)-WKAR(INT3+2)
      FLT3=PNT3(1)-PNT4(1)
      FLT4=PNT3(2)-PNT4(2)
      FLT5=PNT3(1)-WKAR(INT3+1)
      FLT6=PNT3(2)-WKAR(INT3+2)
      FLT7=FLT1*FLT4-FLT2*FLT3
      IF (FLT7.NE.0.0) GO TO 204
  203 HFCU=INT3
      GO TO 202
  204 FLT8=(FLT5*FLT4-FLT6*FLT3)/FLT7
      IF ((FLT8.LT.0.0).OR.(FLT8.GT.1.0)) GO TO 203
      FLT9=(FLT1*FLT6-FLT2*FLT5)/FLT7
      IF ((FLT9.LT.0.0).OR.(FLT9.GT.1.0)) GO TO 203
      IF (((FLT8.LT.TOLR).OR.(FLT8.GT.1.0-TOLR)).AND.
     X  ((FLT9.LT.TOLR).OR.(FLT9.GT.1.0-TOLR))) GO TO 203
      FLT1=PNT3(1)-FLT3*FLT9
      FLT2=PNT3(2)-FLT4*FLT9
      IF (VIND(1).EQ.0) THEN
        PNT3(1)=FLT1
        PNT3(2)=FLT2
      ELSE
        PNT4(1)=FLT1
        PNT4(2)=FLT2
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
  301 RETURN
  302 VFLG=.FALSE.
      GO TO 301
C
      END
      SUBROUTINE UGMES4(PNT1,PNT2,TOLR,WKAR,HFCU,HFFL,EFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                    UPDATE THE HEIGHT FUNCTION                     *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGMESH TO UPDATE THE HEIGHT           *
C *  FUNCTION USING THE GIVEN LINE SEGMENT.                           *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGMES4(PNT1,PNT2,TOLR,WKAR,HFCU,HFFL,EFLG)                *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PNT1  AN END POINT OF THE GIVEN SEGMENT.                       *
C *    PNT2  AN END POINT OF THE GIVEN SEGMENT.                       *
C *    TOLR  A TOLERANCE.                                             *
C *    WKAR  A WORK ARRAY.                                            *
C *    HFCU  THE INDEX OF THE CURRENT HEIGHT FUNCTION ELEMENT.        *
C *    HFFL  THE INDEX OF THE LAST HEIGHT FUNCTION ELEMENT.           *
C *    EFLG  AN ERROR FLAG.                                           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PNT1(2),PNT2(2)
      REAL*4        TOLR
      REAL          WKAR(*)
      INTEGER*4     HFCU,HFFL
      INTEGER       EFLG
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
C
      REAL          XINP(4),YINP(4)
      INTEGER       ILRP,IFRP,NINP
C
      REAL          FLT1
      INTEGER       INT1,INT2,INT3
C
C  UPDATE THE HEIGHT FUNCTION.
      EFLG=0
      FLT1=PNT2(1)-PNT1(1)
      IF (FLT1.EQ.0.0) GO TO 201
      IF (FLT1.LT.0.0) THEN
        FLT1=PNT1(1)
        PNT1(1)=PNT2(1)
        PNT2(1)=FLT1
        FLT1=PNT1(2)
        PNT1(2)=PNT2(2)
        PNT2(2)=FLT1
      END IF
      NINP=0
      ILRP=HFCU
  101 IF (WKAR(ILRP+1).GE.PNT1(1)) THEN
        HFB1=WKAR(ILRP)
        ILRP=HFP1(2)
        GO TO 101
      END IF
  102 HFB1=WKAR(ILRP)
      INT1=HFP1(1)
      FLT1=PNT1(1)-WKAR(INT1+1)
      IF (FLT1.GE.0.0) THEN
        ILRP=INT1
        IF (FLT1.NE.0.0) GO TO 102
        IF (WKAR(ILRP+2).EQ.PNT1(2)) GO TO 104
        GO TO 103
      END IF
      FLT1=((WKAR(ILRP+2)-WKAR(INT1+2))*FLT1/
     X  (WKAR(ILRP+1)-WKAR(INT1+1)))+WKAR(INT1+2)
      IF (ABS(FLT1-PNT1(2)).GT.TOLR) THEN
        NINP=NINP+1
        XINP(NINP)=PNT1(1)
        YINP(NINP)=FLT1
      END IF
  103 NINP=NINP+1
      XINP(NINP)=PNT1(1)
      YINP(NINP)=PNT1(2)
  104 IFRP=ILRP
  105 IF (WKAR(IFRP+1).LE.PNT2(1)) THEN
        HFB1=WKAR(IFRP)
        IFRP=HFP1(1)
        GO TO 105
      END IF
  106 HFB1=WKAR(IFRP)
      INT1=HFP1(2)
      FLT1=PNT2(1)-WKAR(INT1+1)
      IF (FLT1.LE.0.0) THEN
        IFRP=INT1
        IF (FLT1.NE.0.0) GO TO 106
        IF (WKAR(IFRP+2).EQ.PNT2(2)) GO TO 108
        FLT1=PNT2(2)
        GO TO 107
      END IF
      FLT1=((WKAR(IFRP+2)-WKAR(INT1+2))*FLT1/
     X  (WKAR(IFRP+1)-WKAR(INT1+1)))+WKAR(INT1+2)
  107 NINP=NINP+1
      XINP(NINP)=PNT2(1)
      YINP(NINP)=PNT2(2)
      IF (ABS(FLT1-PNT2(2)).GT.TOLR) THEN
        NINP=NINP+1
        XINP(NINP)=PNT2(1)
        YINP(NINP)=FLT1
      END IF
  108 INT1=0
  109 HFB1=WKAR(ILRP)
      ILRP=HFP1(1)
  110 IF (ILRP.NE.IFRP) THEN
        IF (INT1.NE.NINP) THEN
          INT1=INT1+1
          WKAR(ILRP+1)=XINP(INT1)
          WKAR(ILRP+2)=YINP(INT1)
          GO TO 109
        END IF
        HFB1=WKAR(ILRP)
        INT2=HFP1(2)
        INT3=HFP1(1)
        HFB1=WKAR(INT2)
        HFP1(1)=INT3
        WKAR(INT2)=HFB1
        HFB1=WKAR(INT3)
        HFP1(2)=INT2
        WKAR(INT3)=HFB1
        HFP1(1)=HFFL
        WKAR(ILRP)=HFB1
        HFFL=ILRP
        ILRP=INT3
        GO TO 110
      END IF
  111 IF (INT1.NE.NINP) THEN
        IF (HFFL.EQ.0) GO TO 202
        INT2=HFFL
        HFB1=WKAR(INT2)
        HFFL=HFP1(1)
        HFB1=WKAR(ILRP)
        INT3=HFP1(2)
        HFP1(2)=INT2
        WKAR(ILRP)=HFB1
        HFB1=WKAR(INT3)
        HFP1(1)=INT2
        WKAR(INT3)=HFB1
        HFP1(2)=INT3
        HFP1(1)=ILRP
        WKAR(INT2)=HFB1
        INT1=INT1+1
        WKAR(INT2+1)=XINP(INT1)
        WKAR(INT2+2)=YINP(INT1)
        GO TO 111
      END IF
      HFCU=IFRP
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
  202 EFLG=1
      GO TO 201
C
      END

