      SUBROUTINE UGXI01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  DEVICE-DEPENDENT MODULE FOR THE                  *
C *          DEC-WINDOWS SYSTEM IN THE FULLY INTERACTIVE MODE         *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A FULLY         *
C *  INTERACTIVE DISPLAY GRAPHIC DEVICE.                              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
      INCLUDE       '[.include]UGDDACBK.FOR/LIST'
C
      INCLUDE       '[.include]UGDDXXWI.FOR/LIST'
C
      PARAMETER     X$C_COORD_MODE_ORIGIN  = '00000000'X
      PARAMETER     X$C_POLYCOMPLEX        = '00000000'X
      PARAMETER     X$C_INPUT_OUTPUT       = '00000001'X
      PARAMETER     X$C_PSEUDO_COLOR       = '00000003'X
      PARAMETER     X$C_DIRECT_COLOR       = '00000005'X
      PARAMETER     X$C_X_CURSOR           = '00000000'X
      PARAMETER     X$M_CW_BACK_PIXEL      = '00000002'X
      PARAMETER     X$M_GC_BACKGROUND      = '00000008'X
      PARAMETER     X$M_KEY_PRESS          = '00000001'X
      PARAMETER     X$M_BUTTON_PRESS       = '00000004'X
      PARAMETER     X$M_EXPOSURE           = '00008000'X
C
      INTEGER*4     X$OPEN_DISPLAY
      INTEGER*4     X$CREATE_WINDOW
      INTEGER*4     X$CREATE_PIXMAP
      INTEGER*4     X$CREATE_GC
      INTEGER*4     X$CREATE_FONT_CURSOR
      INTEGER*4     X$DEFAULT_SCREEN_OF_DISPLAY
      INTEGER*4     X$ROOT_WINDOW_OF_SCREEN
      INTEGER*4     X$DEFAULT_DEPTH_OF_SCREEN
      INTEGER*4     X$WIDTH_OF_SCREEN
      INTEGER*4     X$HEIGHT_OF_SCREEN
      INTEGER*4     X$WIDTH_MM_OF_SCREEN
      INTEGER*4     X$HEIGHT_MM_OF_SCREEN
      INTEGER*4     X$BLACK_PIXEL_OF_SCREEN
      INTEGER*4     X$WHITE_PIXEL_OF_SCREEN
      INTEGER*4     X$ALLOC_NAMED_COLOR
      INTEGER*4     X$DEFAULT_COLORMAP_OF_SCREEN
      INTEGER*4     X$LOAD_QUERY_FONT
      INTEGER*4     X$SYNCHRONIZE
      INTEGER*4     SYS$BINTIM
      INTEGER*4     SYS$SETIMR
      INTEGER*4     SYS$CANTIM
C
      STRUCTURE     /X$VISUAL/
        INTEGER*4     X$A_VISU_EXT_DATA
        INTEGER*4     X$L_VISU_VISUALID
        INTEGER*4     X$L_VISU_CLASS
        INTEGER*4     X$L_VISU_RED_MASK
        INTEGER*4     X$L_VISU_GREEN_MASK
        INTEGER*4     X$L_VISU_BLUE_MASK
        INTEGER*4     X$L_VISU_BITS_PER_RGB
        INTEGER*4     X$L_VISU_MAP_ENTRIES
      END STRUCTURE
C
      STRUCTURE     /X$SET_WIN_ATTRIBUTES/
        INTEGER*4     X$L_SWDA_BACKGROUND_PIXMAP
        INTEGER*4     X$L_SWDA_BACKGROUND_PIXEL
        INTEGER*4     X$L_SWDA_BORDER_PIXMAP
        INTEGER*4     X$L_SWDA_BORDER_PIXEL
        INTEGER*4     X$L_SWDA_BIT_GRAVITY
        INTEGER*4     X$L_SWDA_WIN_GRAVITY
        INTEGER*4     X$L_SWDA_BACKING_STORE
        INTEGER*4     X$L_SWDA_BACKING_PLANES
        INTEGER*4     X$L_SWDA_BACKING_PIXEL
        INTEGER*4     X$L_SWDA_SAVE_UNDER
        INTEGER*4     X$L_SWDA_EVENT_MASK
        INTEGER*4     X$L_SWDA_DO_NOT_PROPAGATE_MASK
        INTEGER*4     X$L_SWDA_OVERRIDE_REDIRECT
        INTEGER*4     X$L_SWDA_COLORMAP
        INTEGER*4     X$L_SWDA_CURSOR
      END STRUCTURE
C
      STRUCTURE     /X$GC_VALUES/
        INTEGER*4     X$L_GCVL_FUNCTION
        INTEGER*4     X$L_GCVL_PLANE_MASK
        INTEGER*4     X$L_GCVL_FOREGROUND
        INTEGER*4     X$L_GCVL_BACKGROUND
        INTEGER*4     X$L_GCVL_LINE_WIDTH
        INTEGER*4     X$L_GCVL_LINE_STYLE
        INTEGER*4     X$L_GCVL_CAP_STYLE
        INTEGER*4     X$L_GCVL_JOIN_STYLE
        INTEGER*4     X$L_GCVL_FILL_STYLE
        INTEGER*4     X$L_GCVL_FILL_RULE
        INTEGER*4     X$L_GCVL_ARC_MODE
        INTEGER*4     X$L_GCVL_TILE
        INTEGER*4     X$L_GCVL_STIPPLE
        INTEGER*4     X$L_GCVL_TS_X_ORIGIN
        INTEGER*4     X$L_GCVL_TS_Y_ORIGIN
        INTEGER*4     X$L_GCVL_FONT
        INTEGER*4     X$L_GCVL_SUBWINDOW_MODE
        INTEGER*4     X$L_GCVL_GRAPHICS_EXPOSURES
        INTEGER*4     X$L_GCVL_CLIP_X_ORIGIN
        INTEGER*4     X$L_GCVL_CLIP_Y_ORIGIN
        INTEGER*4     X$L_GCVL_CLIP_MASK
        INTEGER*4     X$L_GCVL_DASH_OFFSET
        BYTE          X$B_GCVL_DASHES
      END STRUCTURE
C
      STRUCTURE     /X$COLOR/
        INTEGER*4     X$L_COLR_PIXEL
        INTEGER*2     X$W_COLR_RED
        INTEGER*2     X$W_COLR_GREEN
        INTEGER*2     X$W_COLR_BLUE
        BYTE          X$B_COLR_FLAGS
        BYTE          X$B_COLR_PAD
      END STRUCTURE
C
      STRUCTURE     /X$CHAR_STRUCT/
        INTEGER*2     X$W_CHAR_LBEARING
        INTEGER*2     X$W_CHAR_RBEARING
        INTEGER*2     X$W_CHAR_WIDTH
        INTEGER*2     X$W_CHAR_ASCENT
        INTEGER*2     X$W_CHAR_DESCENT
        INTEGER*2     X$W_CHAR_ATTRIBUTES
      END STRUCTURE
C
      STRUCTURE     /X$FONT_STRUCT/
        INTEGER*4     X$A_FSTR_EXT_DATA
        INTEGER*4     X$L_FSTR_FID
        INTEGER*4     X$L_FSTR_DIRECTION
        INTEGER*4     X$L_FSTR_MIN_CHAR_OR_BYTE2
        INTEGER*4     X$L_FSTR_MAX_CHAR_OR_BYTE2
        INTEGER*4     X$L_FSTR_MIN_BYTE1
        INTEGER*4     X$L_FSTR_MAX_BYTE1
        INTEGER*4     X$L_FSTR_ALL_CHARS_EXIST
        INTEGER*4     X$L_FSTR_DEFAULT_CHAR
        INTEGER*4     X$L_FSTR_N_PROPERTIES
        INTEGER*4     X$A_FSTR_PROPERTIES
        RECORD        /X$CHAR_STRUCT/        X$R_FSTR_MIN_BOUNDS
        RECORD        /X$CHAR_STRUCT/        X$R_FSTR_MAX_BOUNDS
        INTEGER*4     X$A_FSTR_PER_CHAR
        INTEGER*4     X$L_FSTR_ASCENT
        INTEGER*4     X$L_FSTR_DESCENT
      END STRUCTURE
C
      STRUCTURE     /X$POINT/
        UNION
        MAP
          INTEGER*2     X$W_GPNT_X
          INTEGER*2     X$W_GPNT_Y
        END MAP
        MAP
          INTEGER*4     X$L_GPNT
        END MAP
        END UNION
      END STRUCTURE
C
      EXTERNAL      UGXI05,UGXI06
C
      INTEGER*4     INST(50)
      INTEGER*4     EXST(39),EXXO,EXYO,EXXZ,EXYZ,EXSY,EXEX,EXCT
      CHARACTER*64  EXTL,EXCH
      EQUIVALENCE   (EXXO,EXST(1)),       (EXYO,EXST(2)),
     X              (EXXZ,EXST(3)),       (EXYZ,EXST(4)),
     X              (EXSY,EXST(5)),
     X              (EXEX,EXST(6)),
     X              (EXCT,EXST(7)),
     X              (EXTL,EXST(8)),
     X              (EXCH,EXST(24))
C
      RECORD        /X$VISUAL/             XVIS
      RECORD        /X$SET_WIN_ATTRIBUTES/ XSWA
      RECORD        /X$GC_VALUES/          XGCV
      RECORD        /X$COLOR/              XSC1,XSC2
      RECORD        /X$FONT_STRUCT/        XFST
      RECORD        /X$POINT/              XPNT
C
      CHARACTER*40  FNM1(DDXZ2)
      CHARACTER*40  FNM2(DDXZ2)
      INTEGER       FLN1(DDXZ2),FLN2(DDXZ2)
      INTEGER       CSIZ
      CHARACTER*13  CTIM
      INTEGER*4     XTIM(2)
C
      INTEGER       INT1,INT2
C
      DATA          INST/9,2,4, 1, 0,'XORG',
     X                     2,4, 2, 0,'YORG',
     X                     2,4, 3, 0,'XSIZ',
     X                     2,4, 4, 0,'YSIZ',
     X                     1,4, 5, 1,'SYNC',
     X                     2,6, 6, 0,'EXPO','SE  ',
     X                     1,5, 7, 1,'GENC','H   ',
     X                     4,5, 8,64,'TITL','E   ',
     X                     4,7,24,64,'CHAN','NEL '/
      DATA          FNM1/'-DEC-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-BITSTREAM-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-DEC-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-BITSTREAM-TERMINAL-MEDIUM-R-NORMAL-'/
      DATA          FNM2/'-14-140-75-75-C-8-ISO8859-1',
     X                   '-18-180-75-75-C-11-ISO8859-1',
     X                   '-28-280-75-75-C-16-ISO8859-1',
     X                   '-36-360-75-75-C-22-ISO8859-1'/
      DATA          FLN1/30,36,30,36/
      DATA          FLN2/27,28,28,28/
      DATA          CTIM/'0 00:XX:XX.XX'/
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.14)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551,
     X       601,651,701,751),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXXO=-1
      EXYO=-1
      EXXZ=800
      EXYZ=600
      EXSY=0
      EXEX=2
      EXCT=0
      EXTL='Unified Graphics System'
      EXCH=' '
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='XWINDOW '
      DDAIL=3
      DDADM=2
      DDAIC(1)=1
      DDAIC(3)=36
      DDAIC(5)=1
      DDABD(1,1)=0
      DDABD(1,2)=EXXZ
      DDABD(2,1)=0
      DDABD(2,2)=EXYZ
      DDXID='DDA/XI00'
      DO 102 INT1=64,1,-1
        IF (EXCH(INT1:INT1).NE.' ') THEN
          DDXDS=X$OPEN_DISPLAY(EXCH(1:INT1))
          GO TO 103
        END IF
  102 CONTINUE
      DDXDS=X$OPEN_DISPLAY()
  103 IF (DDXDS.EQ.0) GO TO 902
      DDXSC=X$DEFAULT_SCREEN_OF_DISPLAY(DDXDS)
      DDXDP=X$DEFAULT_DEPTH_OF_SCREEN(DDXSC)
      DDABX=0.1*REAL(X$WIDTH_MM_OF_SCREEN(DDXSC))/
     X          REAL(X$WIDTH_OF_SCREEN(DDXSC))
      DDABY=0.1*REAL(X$HEIGHT_MM_OF_SCREEN(DDXSC))/
     X          REAL(X$HEIGHT_OF_SCREEN(DDXSC))
      IF (EXSY.NE.0) THEN
        INT1=X$SYNCHRONIZE(DDXDS,1,INT2)
      END IF
      DDXEX=MIN(3,MAX(0,EXEX))
      DDXCT=EXCT
      IF (EXXO.EQ.-1) THEN
        EXXO=(X$WIDTH_OF_SCREEN(DDXSC)-EXXZ)/2
      END IF
      IF (EXYO.EQ.-1) THEN
        EXYO=(X$HEIGHT_OF_SCREEN(DDXSC)-EXYZ)/2
      END IF
      CALL X$DEFAULT_VISUAL_OF_SCREEN(DDXSC,XVIS)
      INT1=X$M_CW_BACK_PIXEL
      XSWA.X$L_SWDA_BACKGROUND_PIXEL=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXWD=X$CREATE_WINDOW(DDXDS,
     X                      X$ROOT_WINDOW_OF_SCREEN(DDXSC),
     X                      EXXO,EXYO,EXXZ,EXYZ,
     X                      0,DDXDP,X$C_INPUT_OUTPUT,XVIS,INT1,XSWA)
      INT1=X$M_GC_BACKGROUND
      XGCV.X$L_GCVL_BACKGROUND=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXGC=X$CREATE_GC(DDXDS,DDXWD,INT1,XGCV)
      IF ((XVIS.X$L_VISU_CLASS.EQ.X$C_PSEUDO_COLOR).OR.
     X    (XVIS.X$L_VISU_CLASS.EQ.X$C_DIRECT_COLOR)) THEN
        DDXTY=1
      ELSE
        DDXTY=0
      END IF
      DDXXZ=EXXZ
      DDXYZ=EXYZ
      IF (DDXEX.GT.1) THEN
        DDXPM=X$CREATE_PIXMAP(DDXDS,DDXWD,EXXZ,EXYZ,DDXDP)
        CALL X$SELECT_ASYNC_INPUT(DDXDS,DDXWD,X$M_KEY_PRESS.OR.
     X                                        X$M_BUTTON_PRESS.OR.
     X                                        X$M_EXPOSURE,
     X                                        UGXI05,0)
        CALL X$SELECT_INPUT(DDXDS,DDXWD,X$M_KEY_PRESS.OR.
     X                                  X$M_BUTTON_PRESS.OR.
     X                                  X$M_EXPOSURE)
      ELSE
        CALL X$SELECT_ASYNC_INPUT(DDXDS,DDXWD,X$M_KEY_PRESS.OR.
     X                                        X$M_BUTTON_PRESS,
     X                                        UGXI05,0)
        CALL X$SELECT_INPUT(DDXDS,DDXWD,X$M_KEY_PRESS.OR.
     X                                  X$M_BUTTON_PRESS)
      END IF
      DO 104 INT1=64,1,-1
        IF (EXTL(INT1:INT1).NE.' ') THEN
          CALL X$STORE_NAME(DDXDS,DDXWD,EXTL(1:INT1))
          GO TO 105
        END IF
  104 CONTINUE
  105 IF (DDXCT.EQ.0) THEN
        DO 106 INT1=1,DDXZ2
          INT2=X$LOAD_QUERY_FONT(DDXDS,
     X                           FNM1(INT1)(1:FLN1(INT1))//
     X                           FNM2(INT1)(1:FLN2(INT1)),XFST)
          DDXFI(INT1)=XFST.X$L_FSTR_FID
          DDXFW(INT1)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_RBEARING-
     X                XFST.X$R_FSTR_MIN_BOUNDS.X$W_CHAR_LBEARING
          DDXFH(INT1)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_ASCENT
  106   CONTINUE
      ELSE
        INT2=X$LOAD_QUERY_FONT(DDXDS,
     X                         FNM1(DDXZ3)(1:FLN1(DDXZ3))//
     X                         FNM2(DDXZ3)(1:FLN2(DDXZ3)),XFST)
        DDXFI(DDXZ3)=XFST.X$L_FSTR_FID
        DDXFW(DDXZ3)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_RBEARING-
     X               XFST.X$R_FSTR_MIN_BOUNDS.X$W_CHAR_LBEARING
        DDXFH(DDXZ3)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_ASCENT
      END IF
      CALL X$MAP_WINDOW(DDXDS,DDXWD)
      IF (DDXTY.EQ.0) THEN
        DDXCI(1)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
      ELSE
        INT1=X$DEFAULT_COLORMAP_OF_SCREEN(DDXSC)
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'WHITE',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(1)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(1)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'RED',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(2)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(2)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'GREEN',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(3)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(3)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'BLUE',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(4)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(4)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'YELLOW',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(5)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(5)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'MAGENTA',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(6)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(6)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'CYAN',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(7)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(7)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
      END IF
      DDXCI(8)=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXDM=0
      DDXPN=0
      DDXPF=0
      DDXHF=0
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 CALL X$UNMAP_WINDOW(DDXDS,DDXWD)
      IF (DDXCT.EQ.0) THEN
        DO 152 INT1=1,DDXZ2
          CALL X$UNLOAD_FONT(DDXDS,DDXFI(INT1))
  152   CONTINUE
      ELSE
        CALL X$UNLOAD_FONT(DDXDS,DDXFI(DDXZ3))
      END IF
      CALL X$SELECT_INPUT(DDXDS,DDXWD,0)
      CALL X$SYNC(DDXDS,.TRUE.)
      IF (DDXEX.GT.1) THEN
        CALL X$FREE_PIXMAP(DDXDS,DDXPM)
      END IF
      CALL X$FREE_GC(DDXDS,DDXGC)
      CALL X$DESTROY_WINDOW(DDXDS,DDXWD)
      CALL X$CLOSE_DISPLAY(DDXDS)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 CALL UGXI02(3,8,0)
      IF (DDIN(2).EQ.0) THEN
C    CLEAR THE SCREEN.
        CALL X$FILL_RECTANGLE(DDXDS,DDXWD,DDXGC,
     X                        0,0,DDXXZ,DDXYZ)
        IF (DDXEX.GT.1) THEN
          CALL X$FILL_RECTANGLE(DDXDS,DDXPM,DDXGC,
     X                          0,0,DDXXZ,DDXYZ)
        END IF
      ELSE
C    CLEAR THE WINDOW.
        CALL X$FILL_RECTANGLE(DDXDS,DDXWD,DDXGC,
     X                        DDAWD(1,1),DDXYZ-DDAWD(2,2),
     X                        DDAWD(1,2)-DDAWD(1,1)+1,
     X                        DDAWD(2,2)-DDAWD(2,1)+1)
        IF (DDXEX.GT.1) THEN
          CALL X$FILL_RECTANGLE(DDXDS,DDXPM,DDXGC,
     X                          DDAWD(1,1),DDXYZ-DDAWD(2,2),
     X                          DDAWD(1,2)-DDAWD(1,1)+1,
     X                          DDAWD(2,2)-DDAWD(2,1)+1)
        END IF
      END IF
      CALL X$FLUSH(DDXDS)
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 DDXDM=DDIN(4)
      DDXPN=0
      DDXPF=0
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
  351 CALL UGXI03
      IF (DDXEX.GT.2) THEN
        CALL X$COPY_AREA(DDXDS,DDXPM,DDXWD,DDXGC,
     X                   0,0,DDXXZ,DDXYZ,0,0)
      END IF
      CALL X$FLUSH(DDXDS)
      DDXDM=0
      GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
  451 CALL UGXI03
      DDXPN=0
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.4)) GO TO 902
      GO TO (461,471,481,491),INT1
C    INQUIRE ABOUT POINTS.
  461 CALL UGXI02(0,DDIN(4),0)
      DDXPF=0
      GO TO 901
C    INQUIRE ABOUT LINES.
  471 CALL UGXI02(1,DDIN(4),DDIN(7))
      DDXPF=1
      GO TO 901
C    INQUIRE ABOUT TEXT.
  481 IF (DDXCT.NE.0) GO TO 902
      IF ((DDIN(8).GE.5).AND.(DDIN(8).LE.355)) GO TO 902
      IF (DDIN(9).NE.2) THEN
        IF (DDIN(7).LT.(DDXFW(1)/2)) GO TO 902
        IF (DDIN(7).GT.(3*DDXFW(DDXZ2)/2)) GO TO 902
      END IF
      CSIZ=1
      DO 467 INT1=2,DDXZ2
        IF (DDIN(7).GT.((DDXFW(INT1-1)+DDXFW(INT1))/2)) CSIZ=INT1
  467 CONTINUE
      CALL UGXI02(2,DDIN(4),0)
      CALL X$SET_FONT(DDXDS,DDXGC,DDXFI(CSIZ))
      DDEX(2)=-DDXFW(CSIZ)/2
      DDEX(3)=-DDXFH(CSIZ)/2
      DDEX(4)=DDXFW(CSIZ)
      DDEX(5)=0
      GO TO 901
C    INQUIRE ABOUT POLYGON FILL.
  491 CALL UGXI02(3,DDIN(4),0)
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.4)) GO TO 902
      GO TO (511,521,531,541),INT1
C    DISPLAY POINTS.
  511 IF (DDXPN.EQ.DDXZ1) CALL UGXI03
      XPNT.X$W_GPNT_X=DDIN(3)
      XPNT.X$W_GPNT_Y=DDXYZ-DDIN(4)
      DDXPN=DDXPN+1
      DDXPT(DDXPN)=XPNT.X$L_GPNT
      GO TO 901
C    DISPLAY LINES.
  521 IF (DDXPN.EQ.DDXZ1) CALL UGXI03
      IF (DDIN(5).EQ.0) THEN
        CALL UGXI03
        DDXPN=0
      END IF
      XPNT.X$W_GPNT_X=DDIN(3)
      XPNT.X$W_GPNT_Y=DDXYZ-DDIN(4)
      DDXPN=DDXPN+1
      DDXPT(DDXPN)=XPNT.X$L_GPNT
      GO TO 901
C    DISPLAY TEXT.
  531 CALL UGXI04(0,DDST)
      IF (DDXEX.LT.3) THEN
        CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                     DDIN(3),DDXYZ-DDIN(4),DDST)
      END IF
      IF (DDXEX.GT.1) THEN
        CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                     DDIN(3),DDXYZ-DDIN(4),DDST)
      END IF
      GO TO 901
C    DISPLAY POLYGON FILL.
  541 DDXPN=0
      DO 542 INT1=1,DDIN(3)
        XPNT.X$W_GPNT_X=DDIN(2*INT1+2)
        XPNT.X$W_GPNT_Y=DDXYZ-DDIN(2*INT1+3)
        DDXPN=DDXPN+1
        DDXPT(DDXPN)=XPNT.X$L_GPNT
  542 CONTINUE
      IF (DDXEX.LT.3) THEN
        CALL X$FILL_POLYGON(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                      X$C_POLYCOMPLEX,
     X                      X$C_COORD_MODE_ORIGIN)
      END IF
      IF (DDXEX.GT.1) THEN
        CALL X$FILL_POLYGON(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                      X$C_POLYCOMPLEX,
     X                      X$C_COORD_MODE_ORIGIN)
      END IF
      DDXPN=0
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 IF (DDIN(2).NE.1) GO TO 902
      CALL X$BELL(DDXDS,100)
      CALL X$FLUSH(DDXDS)
      GO TO 901
C
C  OPERATION 11: MODIFY THE STATUS OF A CONTROL.
  601 GO TO 901
C
C  OPERATION 12: ENABLE OR DISABLE A CONTROL.
  651 GO TO 901
C
C  OPERATION 13: OBTAIN AN EVENT FROM THE GRAPHIC DEVICE.
  701 DDXKX=DDAKX-(DDXFW(DDXZ3)/2)
      IF (DDXKX.LT.0) DDXKX=0
      INT1=95*DDXXZ/100
      IF (DDXKX.GT.INT1) DDXKX=INT1
      DDXKY=DDAKY-(DDXFH(DDXZ3)/2)
      IF (DDXKY.LT.0) DDXKY=0
      INT1=95*DDXYZ/100
      IF (DDXKY.GT.INT1) DDXKY=INT1
      CALL X$SET_FONT(DDXDS,DDXGC,DDXFI(DDXZ3))
  702 DDXKN=0
      DDXKO=0
      CALL UGXI02(2,1,0)
      CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                   DDXKX,DDXYZ-DDXKY,'_')
      IF (DDXEX.GT.1) THEN
        CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                     DDXKX,DDXYZ-DDXKY,'_')
      END IF
      CALL X$FLUSH(DDXDS)
      DDXHF=-1
      IF (DDIN(2).GT.0) THEN
        INT1=MOD(DDIN(2),6000)
        CALL UGCNVF(REAL(INT1)/100.0,2,CTIM(9:13),INT2)
        IF (CTIM(9:9).EQ.' ') CTIM(9:9)='0'
        INT1=(DDIN(2)-INT1)/6000
        CALL UGCNVF(REAL(INT1),0,CTIM(6:7),INT2)
        IF (CTIM(6:6).EQ.' ') CTIM(6:6)='0'
        INT1=SYS$BINTIM(CTIM,XTIM)
        IF (.NOT.INT1) CALL UGZ001
        INT1=SYS$SETIMR(,XTIM,UGXI06,%VAL(99))
        IF (.NOT.INT1) CALL UGZ001
      END IF
      CALL SYS$HIBER()
      IF ((DDIN(2).GT.0).AND.(DDXHF.NE.1)) THEN
        INT1=SYS$CANTIM(%VAL(99),)
        IF (.NOT.INT1) CALL UGZ001
      END IF
      CALL UGXI02(2,8,0)
      CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                   DDXKX,DDXYZ-DDXKY,DDXKS(1:DDXKN)//'_')
      IF (DDXEX.GT.1) THEN
        CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                     DDXKX,DDXYZ-DDXKY,DDXKS(1:DDXKN)//'_')
      END IF
      CALL X$FLUSH(DDXDS)
      IF (DDXHF.EQ.1) THEN
C    PROCESS A TIME-OUT.
        DDXHF=0
        GO TO 901
      ELSE IF (DDXHF.EQ.2) THEN
C    PROCESS A KEYBOARD EVENT.
        DDXHF=0
        IF (DDABC(1).EQ.0) GO TO 702
        DDEX(1)=1
        DDEX(2)=DDXKN
        DDST(1:DDXKN)=DDXKS(1:DDXKN)
        IF (DDAKF.EQ.0) CALL UGXI04(1,DDST(1:DDXKN))
        GO TO 903
      ELSE IF (DDXHF.EQ.3) THEN
C    PROCESS A BUTTON EVENT.
        DDXHF=0
        IF (DDABC(3).EQ.0) GO TO 702
        DDEX(1)=3
        DDEX(2)=DDXKV
        GO TO 903
      END IF
      DDXHF=0
      GO TO 702
C
C  OPERATION 14: SAMPLE AN INTERACTIVE CONTROL.
  751 IF (DDIN(2).EQ.5) THEN
        INT1=X$CREATE_FONT_CURSOR(DDXDS,X$C_X_CURSOR)
        CALL X$DEFINE_CURSOR(DDXDS,DDXWD,INT1)
        CALL X$FLUSH(DDXDS)
  752   DDXHF=-2
        CALL SYS$HIBER()
        IF (DDXHF.NE.4) GO TO 752
        DDXHF=0
        CALL X$UNDEFINE_CURSOR(DDXDS,DDXWD)
        CALL X$FLUSH(DDXDS)
        DDEX(1)=5
        DDEX(2)=DDXPX
        DDEX(3)=DDXPY
        GO TO 903
      END IF
      GO TO 901
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END
      SUBROUTINE UGXI02(TYPE,COLR,LTYP)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO SET UP THE GRAPHIC CONTEXT FOR A      *
C *  GRAPHIC PRIMITIVE.                                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI02(TYPE,COLR,LTYP)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    TYPE  THE TYPE OF THE PRIMITIVE.                               *
C *            0 MEANS POINTS,                                        *
C *            1 MEANS LINES,                                         *
C *            2 MEANS TEXT, AND                                      *
C *            3 MEANS POLYGON FILL.                                  *
C *    COLR  THE COLOR OF THE PRIMITIVE.                              *
C *    LTYP  THE LINE TYPE FOR A LINE PRIMITIVE.                      *
C *                                                                   *
C *********************************************************************
C
      INTEGER       TYPE,COLR,LTYP
C
      INCLUDE       '[.include]UGDDXXWI.FOR/NOLIST'
C
      PARAMETER     X$C_LINE_SOLID         = '00000000'X
      PARAMETER     X$C_LINE_ON_OFF_DASH   = '00000001'X
      PARAMETER     X$C_CAP_BUTT           = '00000001'X
      PARAMETER     X$C_JOIN_MITER         = '00000000'X
C
      INTEGER       ACLR,ALTP
      BYTE          DASH(2),DOTS(2),DDSH(4)
C
      DATA          DASH/13,13/
      DATA          DOTS/1,9/
      DATA          DDSH/9,9,1,9/
C
C  SET THE COLOR IN THE GRAPHIC CONTEXT.
      IF (DDXDM.NE.0) THEN
        ACLR=8
      ELSE
        ACLR=COLR
      END IF
      IF (DDXTY.EQ.0) THEN
        IF (ACLR.NE.8) ACLR=1
      END IF
      CALL X$SET_FOREGROUND(DDXDS,DDXGC,DDXCI(ACLR))
C
C  SET THE LINE STRUCTURE IN THE GRAPHIC CONTEXT.
      IF (TYPE.NE.1) THEN
        ALTP=1
      ELSE
        ALTP=LTYP
      END IF
      IF (ALTP.EQ.2) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DASH,2)
      ELSE IF (ALTP.EQ.3) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DOTS,2)
      ELSE IF (ALTP.EQ.4) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DDSH,4)
      ELSE
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_SOLID,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGXI03
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO WRITE OUT THE ACCUMULATED CONTENTS    *
C *  OF THE POINT/LINE END POINT ARRAY.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI03                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE       '[.include]UGDDXXWI.FOR/NOLIST'
C
      PARAMETER     X$C_COORD_MODE_ORIGIN  = '00000000'X
C
C  WRITE OUT THE ACCUMULATED POINTS AND RE-INITIALIZE THE POINT
C  ARRAY.
      IF (DDXPF.EQ.0) THEN
C    WRITE OUT POINT DATA.
        IF (DDXPN.GT.0) THEN
          IF (DDXEX.LT.3) THEN
            CALL X$DRAW_POINTS(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                         X$C_COORD_MODE_ORIGIN)
          END IF
          IF (DDXEX.GT.1) THEN
            CALL X$DRAW_POINTS(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                         X$C_COORD_MODE_ORIGIN)
          END IF
        END IF
        DDXPN=0
      ELSE
C    WRITE OUT LINE DATA.
        IF (DDXPN.GT.1) THEN
          IF (DDXEX.LT.3) THEN
            CALL X$DRAW_LINES(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                        X$C_COORD_MODE_ORIGIN)
          END IF
          IF (DDXEX.GT.1) THEN
            CALL X$DRAW_LINES(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                        X$C_COORD_MODE_ORIGIN)
          END IF
        END IF
        DDXPT(1)=DDXPT(DDXZ1)
        DDXPN=1
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGXI04(FLAG,STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TRANSLATE CHARACTER STRINGS FROM THE  *
C *  COMPUTER CHARACTER SET TO THE DISPLAY CHARACTER SET, OR TO       *
C *  UPPER CASE IN THE COMPUTER CHARACTER SET.                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI04(FLAG,STRG)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  TRANSLATE FLAG (0 MEANS TERMINAL TO COMPUTER, 1 MEANS    *
C *          UPPER CASE).                                             *
C *    STRG  THE CHARACTER STRING TO BE TRANSLATED.                   *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      CHARACTER*(*) STRG
C
      INTEGER       INT1,INT2
      CHARACTER*1   CHR1
C
C  TRANSLATE THE CHARACTER STRING AS NEEDED.
      IF (FLAG.EQ.0) THEN
        DO 101 INT1=1,LEN(STRG)
          CHR1=STRG(INT1:INT1)
          IF ((CHR1.LT.' ').OR.(CHR1.GT.'~')) STRG(INT1:INT1)='@'
  101   CONTINUE
      ELSE IF (FLAG.EQ.1) THEN
        DO 102 INT1=1,LEN(STRG)
          INT2=ICHAR(STRG(INT1:INT1))
          IF ((INT2.GE.ICHAR('a')).AND.(INT2.LE.ICHAR('z')))
     X      STRG(INT1:INT1)=CHAR(INT2-ICHAR('a')+ICHAR('A'))
  102   CONTINUE
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGXI05(ARGU)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS AN ASYNCHRONOUS SYSTEM TRAP (AST) THAT IS     *
C *  ENTERED WHENEVER A "KEY PRESS", "BUTTON PRESS" OR "EXPOSE"       *
C *  EVENT OCCURS.                                                    *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI05(ARGU)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    ARGU  THE ARGUMENT SUPPLIED TO THE SUBROUTINE.                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER*4     ARGU
C
      INCLUDE       '[.include]UGDDACBK.FOR/NOLIST'
C
      INCLUDE       '[.include]UGDDXXWI.FOR/NOLIST'
C
      PARAMETER     X$C_KEY_PRESS          = '00000002'X
      PARAMETER     X$C_BUTTON_PRESS       = '00000004'X
      PARAMETER     X$C_EXPOSE             = '0000000C'X
C
      INTEGER*4     X$PENDING
      INTEGER*4     X$LOOKUP_STRING
C
      STRUCTURE     /X$KEY_EVENT/
        INTEGER*4     X$L_KYEV_TYPE
        INTEGER*4     X$L_KYEV_SERIAL
        INTEGER*4     X$L_KYEV_SEND_EVENT
        INTEGER*4     X$A_KYEV_DISPLAY
        INTEGER*4     X$L_KYEV_WINDOW
        INTEGER*4     X$L_KYEV_ROOT
        INTEGER*4     X$L_KYEV_SUBWINDOW
        INTEGER*4     X$L_KYEV_TIME
        INTEGER*4     X$L_KYEV_X
        INTEGER*4     X$L_KYEV_Y
        INTEGER*4     X$L_KYEV_X_ROOT
        INTEGER*4     X$L_KYEV_Y_ROOT
        INTEGER*4     X$L_KYEV_STATE
        INTEGER*4     X$L_KYEV_KEYCODE
        INTEGER*4     X$L_KYEV_SAME_SCREEN
      END STRUCTURE
C
      STRUCTURE     /X$BUTTON_EVENT/
        INTEGER*4     X$L_BTEV_TYPE
        INTEGER*4     X$L_BTEV_SERIAL
        INTEGER*4     X$L_BTEV_SEND_EVENT
        INTEGER*4     X$A_BTEV_DISPLAY
        INTEGER*4     X$L_BTEV_WINDOW
        INTEGER*4     X$L_BTEV_ROOT
        INTEGER*4     X$L_BTEV_SUBWINDOW
        INTEGER*4     X$L_BTEV_TIME
        INTEGER*4     X$L_BTEV_X
        INTEGER*4     X$L_BTEV_Y
        INTEGER*4     X$L_BTEV_X_ROOT
        INTEGER*4     X$L_BTEV_Y_ROOT
        INTEGER*4     X$L_BTEV_STATE
        INTEGER*4     X$L_BTEV_BUTTON
        INTEGER*4     X$L_BTEV_SAME_SCREEN
      END STRUCTURE
C
      STRUCTURE     /X$EXPOSE_EVENT/
        INTEGER*4     X$L_EXEV_TYPE
        INTEGER*4     X$L_EXEV_SERIAL
        INTEGER*4     X$L_EXEV_SEND_EVENT
        INTEGER*4     X$A_EXEV_DISPLAY
        INTEGER*4     X$L_EXEV_WINDOW
        INTEGER*4     X$L_EXEV_X
        INTEGER*4     X$L_EXEV_Y
        INTEGER*4     X$L_EXEV_WIDTH
        INTEGER*4     X$L_EXEV_HEIGHT
        INTEGER*4     X$L_EXEV_COUNT
      END STRUCTURE
C
      STRUCTURE     /X$EVENT/
        UNION
        MAP
          INTEGER*4     EVNT_TYPE
        END MAP
        MAP
          RECORD        /X$KEY_EVENT/          EVNT_KEY
        END MAP
        MAP
          RECORD        /X$BUTTON_EVENT/       EVNT_BUTTON
        END MAP
        MAP
          RECORD        /X$EXPOSE_EVENT/       EVNT_EXPOSE
        END MAP
        MAP
          INTEGER*4     EVNT_PAD(1:24)
        END MAP
        END UNION
      END STRUCTURE
C
      RECORD        /X$EVENT/              XEVT
C
      INTEGER*4     CHIN
      CHARACTER*4   CHST
      INTEGER       BTVL
C
      INTEGER       INT1
C
C  PROCESS ANY WAITING EVENTS.
  101 IF (X$PENDING(DDXDS).GT.0) THEN
C    OBTAIN THE NEXT AVAILABLE EVENT.
        CALL X$NEXT_EVENT(DDXDS,XEVT)
        IF (XEVT.EVNT_TYPE.EQ.X$C_KEY_PRESS) THEN
C    PROCESS A KEY PRESS EVENT.
          IF (DDXHF.EQ.-1) THEN
            INT1=X$LOOKUP_STRING(XEVT,CHST,4,CHIN,)
            IF (CHIN.EQ.'0000FF0D'X) THEN
C      PROCESS A CARRIAGE RETURN.
              DDXHF=2
              CALL SYS$WAKE(,)
            ELSE IF ((CHIN.LT.'0000007F'X).AND.
     X               (CHIN.GT.'0000001F'X).AND.
     X               (CHST(1:1).EQ.CHAR(CHIN))) THEN
C      PROCESS A PRINTABLE CHARACTER.
              IF (DDXKN.GE.DDAKN) THEN
                CALL X$BELL(DDXDS,100)
              ELSE
                CALL UGXI02(2,8,0)
                CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                             DDXKX+DDXFW(DDXZ3)*DDXKN,
     X                             DDXYZ-DDXKY,'_')
                IF (DDXEX.GT.1) THEN
                  CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                               DDXKX+DDXFW(DDXZ3)*DDXKN,
     X                               DDXYZ-DDXKY,'_')
                END IF
                DDXKN=DDXKN+1
                DDXKS(DDXKN:DDXKN)=CHST(1:1)
                CALL UGXI02(2,1,0)
                CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                             DDXKX+DDXFW(DDXZ3)*(DDXKN-1),
     X                             DDXYZ-DDXKY,
     X                             DDXKS(DDXKN:DDXKN)//'_')
                IF (DDXEX.GT.1) THEN
                  CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                               DDXKX+DDXFW(DDXZ3)*(DDXKN-1),
     X                               DDXYZ-DDXKY,
     X                               DDXKS(DDXKN:DDXKN)//'_')
                END IF
              END IF
              CALL X$FLUSH(DDXDS)
            ELSE IF (((CHST(1:1).EQ.CHAR('08'X)).AND.
     X                (CHIN.EQ.'00000068'X)).OR.
     X               ((CHST(1:1).EQ.CHAR('7F'X)).AND.
     X                (CHIN.EQ.'0000FFFF'X))) THEN
C      PROCESS A BACKSPACE OR DELETE.
              IF (DDXKN.EQ.0) THEN
                CALL X$BELL(DDXDS,100)
              ELSE
                CALL UGXI02(2,8,0)
                CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                             DDXKX+DDXFW(DDXZ3)*(DDXKN-1),
     X                             DDXYZ-DDXKY,
     X                             DDXKS(DDXKN:DDXKN)//'_')
                IF (DDXEX.GT.1) THEN
                  CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                               DDXKX+DDXFW(DDXZ3)*(DDXKN-1),
     X                               DDXYZ-DDXKY,
     X                               DDXKS(DDXKN:DDXKN)//'_')
                END IF
                DDXKN=DDXKN-1
                CALL UGXI02(2,1,0)
                CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                             DDXKX+DDXFW(DDXZ3)*DDXKN,
     X                             DDXYZ-DDXKY,'_')
                IF (DDXEX.GT.1) THEN
                  CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                               DDXKX+DDXFW(DDXZ3)*DDXKN,
     X                               DDXYZ-DDXKY,'_')
                END IF
              END IF
              CALL X$FLUSH(DDXDS)
            ELSE IF (CHIN.EQ.'0000FFB0'X) THEN
C      PROCESS A FUNCTION BUTTON.
              DDXKO=12
              GO TO 201
            ELSE IF (CHIN.EQ.'0000FFAC'X) THEN
              DDXKO=24
              GO TO 201
            ELSE IF (CHIN.EQ.'0000FF91'X) THEN
              BTVL=1
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FF92'X) THEN
              BTVL=2
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FF93'X) THEN
              BTVL=3
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB7'X) THEN
              BTVL=4
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB8'X) THEN
              BTVL=5
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB9'X) THEN
              BTVL=6
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB4'X) THEN
              BTVL=7
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB5'X) THEN
              BTVL=8
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB6'X) THEN
              BTVL=9
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB1'X) THEN
              BTVL=10
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB2'X) THEN
              BTVL=11
              GO TO 102
            ELSE IF (CHIN.EQ.'0000FFB3'X) THEN
              BTVL=12
  102         DDXKV=DDXKO+BTVL
              DDXHF=3
              CALL SYS$WAKE(,)
            END IF
          END IF
        ELSE IF (XEVT.EVNT_TYPE.EQ.X$C_BUTTON_PRESS) THEN
C    PROCESS A BUTTON PRESS EVENT.
          IF (DDXHF.EQ.-2) THEN
            DDXPX=XEVT.EVNT_BUTTON.X$L_BTEV_X
            DDXPY=DDXYZ-XEVT.EVNT_BUTTON.X$L_BTEV_Y
            DDXHF=4
            CALL SYS$WAKE(,)
          END IF
        ELSE IF (XEVT.EVNT_TYPE.EQ.X$C_EXPOSE) THEN
C    PROCESS AN EXPOSURE EVENT.
          CALL X$COPY_AREA(DDXDS,DDXPM,DDXWD,DDXGC,
     X                     0,0,DDXXZ,DDXYZ,0,0)
          CALL X$FLUSH(DDXDS)
        ELSE
C    SKIP THE EVENT AND TRY AGAIN.
          GO TO 101
        END IF
      END IF
      DDXKO=0
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C
      END
      SUBROUTINE UGXI06
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS AN ASYNCHRONOUS SYSTEM TRAP (AST) FOR THE     *
C *  TIMER.  WHEN ENABLED, IT IS ENTERED WHENEVER THE TIME INTERVAL   *
C *  ESTABLISHED IN UGXI01 EXPIRES.                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXI06                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE       '[.include]UGDDXXWI.FOR/NOLIST'
C
C  PROCESS A TIME-OUT.
      IF (DDXHF.EQ.-1) THEN
        DDXHF=1
        CALL SYS$WAKE(,)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
