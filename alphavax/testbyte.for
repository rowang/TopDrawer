      integer * 4 inum
      character * 1 str(4)
      equivalence (inum,str,rnum)
c system dependent values for masks.
c msk1 must have only one bit on, msk2 = inot(msk1),
c and the relevant bit must be irrelevant in real *4 numbers
c       DATA          MSK1/ 1/
c       DATA          MSK2/-2/
      data msk1/z00010000/
      data msk2/zfffeffff/
c 1     write(*,*) ' enter inum'
c      read(*,*) inum
c      write(*,*) (ichar(str(k)),k=1,4),rnum
c      inum=msk1
c      write(*,*) (ichar(str(k)),k=1,4),rnum
c      inum=msk2
c      write(*,*) (ichar(str(k)),k=1,4),rnum
      iseed=12341452
      do j=1,100
         real=ran(iseed)
         rnum=real
         inum=or(inum,msk1)
         write(*,*) real/rnum,real,rnum
      enddo
      end

