      SUBROUTINE UGXS01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  DEVICE-DEPENDENT MODULE FOR THE                  *
C *            DEC-WINDOWS SYSTEM IN A SLAVE DISPLAY MODE             *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A SLAVE         *
C *  DISPLAY GRAPHIC DEVICE.                                          *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXS01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
      INCLUDE       '[.include]UGDDACBK.FOR/LIST'
C
      INCLUDE       '[.include]UGDDXXWS.FOR/LIST'
C
      PARAMETER     X$C_COORD_MODE_ORIGIN  = '00000000'X
      PARAMETER     X$C_POLYCOMPLEX        = '00000000'X
      PARAMETER     X$C_INPUT_OUTPUT       = '00000001'X
      PARAMETER     X$C_PSEUDO_COLOR       = '00000003'X
      PARAMETER     X$C_DIRECT_COLOR       = '00000005'X
      PARAMETER     X$M_CW_BACK_PIXEL      = '00000002'X
      PARAMETER     X$M_GC_BACKGROUND      = '00000008'X
      PARAMETER     X$M_EXPOSURE           = '00008000'X
C
      INTEGER*4     X$OPEN_DISPLAY
      INTEGER*4     X$CREATE_WINDOW
      INTEGER*4     X$CREATE_PIXMAP
      INTEGER*4     X$CREATE_GC
      INTEGER*4     X$DEFAULT_SCREEN_OF_DISPLAY
      INTEGER*4     X$ROOT_WINDOW_OF_SCREEN
      INTEGER*4     X$DEFAULT_DEPTH_OF_SCREEN
      INTEGER*4     X$WIDTH_OF_SCREEN
      INTEGER*4     X$HEIGHT_OF_SCREEN
      INTEGER*4     X$WIDTH_MM_OF_SCREEN
      INTEGER*4     X$HEIGHT_MM_OF_SCREEN
      INTEGER*4     X$BLACK_PIXEL_OF_SCREEN
      INTEGER*4     X$WHITE_PIXEL_OF_SCREEN
      INTEGER*4     X$ALLOC_NAMED_COLOR
      INTEGER*4     X$DEFAULT_COLORMAP_OF_SCREEN
      INTEGER*4     X$LOAD_QUERY_FONT
      INTEGER*4     X$SYNCHRONIZE
C
      STRUCTURE     /X$VISUAL/
        INTEGER*4     X$A_VISU_EXT_DATA
        INTEGER*4     X$L_VISU_VISUALID
        INTEGER*4     X$L_VISU_CLASS
        INTEGER*4     X$L_VISU_RED_MASK
        INTEGER*4     X$L_VISU_GREEN_MASK
        INTEGER*4     X$L_VISU_BLUE_MASK
        INTEGER*4     X$L_VISU_BITS_PER_RGB
        INTEGER*4     X$L_VISU_MAP_ENTRIES
      END STRUCTURE
C
      STRUCTURE     /X$SET_WIN_ATTRIBUTES/
        INTEGER*4     X$L_SWDA_BACKGROUND_PIXMAP
        INTEGER*4     X$L_SWDA_BACKGROUND_PIXEL
        INTEGER*4     X$L_SWDA_BORDER_PIXMAP
        INTEGER*4     X$L_SWDA_BORDER_PIXEL
        INTEGER*4     X$L_SWDA_BIT_GRAVITY
        INTEGER*4     X$L_SWDA_WIN_GRAVITY
        INTEGER*4     X$L_SWDA_BACKING_STORE
        INTEGER*4     X$L_SWDA_BACKING_PLANES
        INTEGER*4     X$L_SWDA_BACKING_PIXEL
        INTEGER*4     X$L_SWDA_SAVE_UNDER
        INTEGER*4     X$L_SWDA_EVENT_MASK
        INTEGER*4     X$L_SWDA_DO_NOT_PROPAGATE_MASK
        INTEGER*4     X$L_SWDA_OVERRIDE_REDIRECT
        INTEGER*4     X$L_SWDA_COLORMAP
        INTEGER*4     X$L_SWDA_CURSOR
      END STRUCTURE
C
      STRUCTURE     /X$GC_VALUES/
        INTEGER*4     X$L_GCVL_FUNCTION
        INTEGER*4     X$L_GCVL_PLANE_MASK
        INTEGER*4     X$L_GCVL_FOREGROUND
        INTEGER*4     X$L_GCVL_BACKGROUND
        INTEGER*4     X$L_GCVL_LINE_WIDTH
        INTEGER*4     X$L_GCVL_LINE_STYLE
        INTEGER*4     X$L_GCVL_CAP_STYLE
        INTEGER*4     X$L_GCVL_JOIN_STYLE
        INTEGER*4     X$L_GCVL_FILL_STYLE
        INTEGER*4     X$L_GCVL_FILL_RULE
        INTEGER*4     X$L_GCVL_ARC_MODE
        INTEGER*4     X$L_GCVL_TILE
        INTEGER*4     X$L_GCVL_STIPPLE
        INTEGER*4     X$L_GCVL_TS_X_ORIGIN
        INTEGER*4     X$L_GCVL_TS_Y_ORIGIN
        INTEGER*4     X$L_GCVL_FONT
        INTEGER*4     X$L_GCVL_SUBWINDOW_MODE
        INTEGER*4     X$L_GCVL_GRAPHICS_EXPOSURES
        INTEGER*4     X$L_GCVL_CLIP_X_ORIGIN
        INTEGER*4     X$L_GCVL_CLIP_Y_ORIGIN
        INTEGER*4     X$L_GCVL_CLIP_MASK
        INTEGER*4     X$L_GCVL_DASH_OFFSET
        BYTE          X$B_GCVL_DASHES
      END STRUCTURE
C
      STRUCTURE     /X$COLOR/
        INTEGER*4     X$L_COLR_PIXEL
        INTEGER*2     X$W_COLR_RED
        INTEGER*2     X$W_COLR_GREEN
        INTEGER*2     X$W_COLR_BLUE
        BYTE          X$B_COLR_FLAGS
        BYTE          X$B_COLR_PAD
      END STRUCTURE
C
      STRUCTURE     /X$CHAR_STRUCT/
        INTEGER*2     X$W_CHAR_LBEARING
        INTEGER*2     X$W_CHAR_RBEARING
        INTEGER*2     X$W_CHAR_WIDTH
        INTEGER*2     X$W_CHAR_ASCENT
        INTEGER*2     X$W_CHAR_DESCENT
        INTEGER*2     X$W_CHAR_ATTRIBUTES
      END STRUCTURE
C
      STRUCTURE     /X$FONT_STRUCT/
        INTEGER*4     X$A_FSTR_EXT_DATA
        INTEGER*4     X$L_FSTR_FID
        INTEGER*4     X$L_FSTR_DIRECTION
        INTEGER*4     X$L_FSTR_MIN_CHAR_OR_BYTE2
        INTEGER*4     X$L_FSTR_MAX_CHAR_OR_BYTE2
        INTEGER*4     X$L_FSTR_MIN_BYTE1
        INTEGER*4     X$L_FSTR_MAX_BYTE1
        INTEGER*4     X$L_FSTR_ALL_CHARS_EXIST
        INTEGER*4     X$L_FSTR_DEFAULT_CHAR
        INTEGER*4     X$L_FSTR_N_PROPERTIES
        INTEGER*4     X$A_FSTR_PROPERTIES
        RECORD        /X$CHAR_STRUCT/        X$R_FSTR_MIN_BOUNDS
        RECORD        /X$CHAR_STRUCT/        X$R_FSTR_MAX_BOUNDS
        INTEGER*4     X$A_FSTR_PER_CHAR
        INTEGER*4     X$L_FSTR_ASCENT
        INTEGER*4     X$L_FSTR_DESCENT
      END STRUCTURE
C
      STRUCTURE     /X$POINT/
        UNION
        MAP
          INTEGER*2     X$W_GPNT_X
          INTEGER*2     X$W_GPNT_Y
        END MAP
        MAP
          INTEGER*4     X$L_GPNT
        END MAP
        END UNION
      END STRUCTURE
C
      EXTERNAL      UGB001
      EXTERNAL      UGXS05
C
      INTEGER*4     INST(50)
      INTEGER*4     EXST(39),EXXO,EXYO,EXXZ,EXYZ,EXSY,EXEX,EXCT
      CHARACTER*64  EXTL,EXCH
      EQUIVALENCE   (EXXO,EXST(1)),       (EXYO,EXST(2)),
     X              (EXXZ,EXST(3)),       (EXYZ,EXST(4)),
     X              (EXSY,EXST(5)),
     X              (EXEX,EXST(6)),
     X              (EXCT,EXST(7)),
     X              (EXTL,EXST(8)),
     X              (EXCH,EXST(24))
C
      RECORD        /X$VISUAL/             XVIS
      RECORD        /X$SET_WIN_ATTRIBUTES/ XSWA
      RECORD        /X$GC_VALUES/          XGCV
      RECORD        /X$COLOR/              XSC1,XSC2
      RECORD        /X$FONT_STRUCT/        XFST
      RECORD        /X$POINT/              XPNT
C
      INTEGER       B001
      CHARACTER*40  FNM1(DDXZ2)
      CHARACTER*40  FNM2(DDXZ2)
      INTEGER       FLN1(DDXZ2),FLN2(DDXZ2)
      INTEGER       CSIZ
C
      INTEGER       INT1,INT2
C
      DATA          INST/9,2,4, 1, 0,'XORG',
     X                     2,4, 2, 0,'YORG',
     X                     2,4, 3, 0,'XSIZ',
     X                     2,4, 4, 0,'YSIZ',
     X                     1,4, 5, 1,'SYNC',
     X                     2,6, 6, 0,'EXPO','SE  ',
     X                     1,5, 7, 1,'GENC','H   ',
     X                     4,5, 8,64,'TITL','E   ',
     X                     4,7,24,64,'CHAN','NEL '/
      DATA          FNM1/'-DEC-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-BITSTREAM-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-DEC-TERMINAL-MEDIUM-R-NORMAL-',
     X                   '-BITSTREAM-TERMINAL-MEDIUM-R-NORMAL-'/
      DATA          FNM2/'-14-140-75-75-C-8-ISO8859-1',
     X                   '-18-180-75-75-C-11-ISO8859-1',
     X                   '-28-280-75-75-C-16-ISO8859-1',
     X                   '-36-360-75-75-C-22-ISO8859-1'/
      DATA          FLN1/30,36,30,36/
      DATA          FLN2/27,28,28,28/
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.10)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXXO=-1
      EXYO=-1
      EXXZ=800
      EXYZ=600
      EXSY=0
      EXEX=2
      EXCT=0
      EXTL='Unified Graphics System'
      EXCH=' '
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='SDDXWDO '
      DDAIL=2
      DDADM=2
      DDABD(1,1)=0
      DDABD(1,2)=EXXZ
      DDABD(2,1)=0
      DDABD(2,2)=EXYZ
      DDXID='DDA/XS00'
      DO 102 INT1=64,1,-1
        IF (EXCH(INT1:INT1).NE.' ') THEN
          DDXDS=X$OPEN_DISPLAY(EXCH(1:INT1))
          GO TO 103
        END IF
  102 CONTINUE
      DDXDS=X$OPEN_DISPLAY()
  103 IF (DDXDS.EQ.0) GO TO 902
      DDXSC=X$DEFAULT_SCREEN_OF_DISPLAY(DDXDS)
      DDXDP=X$DEFAULT_DEPTH_OF_SCREEN(DDXSC)
      DDABX=0.1*REAL(X$WIDTH_MM_OF_SCREEN(DDXSC))/
     X          REAL(X$WIDTH_OF_SCREEN(DDXSC))
      DDABY=0.1*REAL(X$HEIGHT_MM_OF_SCREEN(DDXSC))/
     X          REAL(X$HEIGHT_OF_SCREEN(DDXSC))
      IF (EXSY.NE.0) THEN
        INT1=X$SYNCHRONIZE(DDXDS,1,INT2)
      END IF
      DDXEX=MIN(3,MAX(0,EXEX))
      DDXCT=EXCT
      IF (EXXO.EQ.-1) THEN
        EXXO=(X$WIDTH_OF_SCREEN(DDXSC)-EXXZ)/2
      END IF
      IF (EXYO.EQ.-1) THEN
        EXYO=(X$HEIGHT_OF_SCREEN(DDXSC)-EXYZ)/2
      END IF
      CALL X$DEFAULT_VISUAL_OF_SCREEN(DDXSC,XVIS)
      INT1=X$M_CW_BACK_PIXEL
      XSWA.X$L_SWDA_BACKGROUND_PIXEL=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXWD=X$CREATE_WINDOW(DDXDS,
     X                      X$ROOT_WINDOW_OF_SCREEN(DDXSC),
     X                      EXXO,EXYO,EXXZ,EXYZ,
     X                      0,DDXDP,X$C_INPUT_OUTPUT,XVIS,INT1,XSWA)
      INT1=X$M_GC_BACKGROUND
      XGCV.X$L_GCVL_BACKGROUND=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXGC=X$CREATE_GC(DDXDS,DDXWD,INT1,XGCV)
      IF ((XVIS.X$L_VISU_CLASS.EQ.X$C_PSEUDO_COLOR).OR.
     X    (XVIS.X$L_VISU_CLASS.EQ.X$C_DIRECT_COLOR)) THEN
        DDXTY=1
      ELSE
        DDXTY=0
      END IF
      DDXXZ=EXXZ
      DDXYZ=EXYZ
      IF (DDXEX.GT.1) THEN
        DDXPM=X$CREATE_PIXMAP(DDXDS,DDXWD,EXXZ,EXYZ,DDXDP)
        CALL UGZ003(0,8,DDXPD)
        CALL UGZ004(UGB001,B001)
        CALL UGZ007(B001,1,1,DDXPD,1,DDXDS,1,1)
        CALL UGZ007(B001,1,1,DDXPD,2,DDXWD,1,1)
        CALL UGZ007(B001,1,1,DDXPD,3,DDXGC,1,1)
        CALL UGZ007(B001,1,1,DDXPD,4,DDXPM,1,1)
        CALL UGZ007(B001,1,1,DDXPD,5,DDXXZ,1,1)
        CALL UGZ007(B001,1,1,DDXPD,6,DDXYZ,1,1)
        CALL X$SELECT_ASYNC_INPUT(DDXDS,DDXWD,X$M_EXPOSURE,UGXS05,DDXPD)
        CALL X$SELECT_INPUT(DDXDS,DDXWD,X$M_EXPOSURE)
      END IF
      DO 104 INT1=64,1,-1
        IF (EXTL(INT1:INT1).NE.' ') THEN
          CALL X$STORE_NAME(DDXDS,DDXWD,EXTL(1:INT1))
          GO TO 105
        END IF
  104 CONTINUE
  105 IF (DDXCT.EQ.0) THEN
        DO 106 INT1=1,DDXZ2
          INT2=X$LOAD_QUERY_FONT(DDXDS,
     X                           FNM1(INT1)(1:FLN1(INT1))//
     X                           FNM2(INT1)(1:FLN2(INT1)),XFST)
          DDXFI(INT1)=XFST.X$L_FSTR_FID
          DDXFW(INT1)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_RBEARING-
     X                XFST.X$R_FSTR_MIN_BOUNDS.X$W_CHAR_LBEARING
          DDXFH(INT1)=XFST.X$R_FSTR_MAX_BOUNDS.X$W_CHAR_ASCENT
  106   CONTINUE
      END IF
      CALL X$MAP_WINDOW(DDXDS,DDXWD)
      IF (DDXTY.EQ.0) THEN
        DDXCI(1)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
      ELSE
        INT1=X$DEFAULT_COLORMAP_OF_SCREEN(DDXSC)
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'WHITE',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(1)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(1)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'RED',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(2)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(2)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'GREEN',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(3)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(3)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'BLUE',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(4)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(4)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'YELLOW',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(5)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(5)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'MAGENTA',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(6)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(6)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
        INT2=X$ALLOC_NAMED_COLOR(DDXDS,INT1,'CYAN',XSC1,XSC2)
        IF (INT2.NE.0) THEN
          DDXCI(7)=XSC1.X$L_COLR_PIXEL
        ELSE
          DDXCI(7)=X$WHITE_PIXEL_OF_SCREEN(DDXSC)
        END IF
      END IF
      DDXCI(8)=X$BLACK_PIXEL_OF_SCREEN(DDXSC)
      DDXDM=0
      DDXPN=0
      DDXPF=0
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 CALL X$UNMAP_WINDOW(DDXDS,DDXWD)
      IF (DDXCT.EQ.0) THEN
        DO 152 INT1=1,DDXZ2
          CALL X$UNLOAD_FONT(DDXDS,DDXFI(INT1))
  152   CONTINUE
      END IF
      IF (DDXEX.GT.1) THEN
        CALL X$SELECT_INPUT(DDXDS,DDXWD,0)
        CALL X$SYNC(DDXDS,.TRUE.)
        CALL X$FREE_PIXMAP(DDXDS,DDXPM)
        CALL UGZ003(1,8,DDXPD)
      END IF
      CALL X$FREE_GC(DDXDS,DDXGC)
      CALL X$DESTROY_WINDOW(DDXDS,DDXWD)
      CALL X$CLOSE_DISPLAY(DDXDS)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 CALL UGXS02(3,8,0)
      IF (DDIN(2).EQ.0) THEN
C    CLEAR THE SCREEN.
        CALL X$FILL_RECTANGLE(DDXDS,DDXWD,DDXGC,
     X                        0,0,DDXXZ,DDXYZ)
        IF (DDXEX.GT.1) THEN
          CALL X$FILL_RECTANGLE(DDXDS,DDXPM,DDXGC,
     X                          0,0,DDXXZ,DDXYZ)
        END IF
      ELSE
C    CLEAR THE WINDOW.
        CALL X$FILL_RECTANGLE(DDXDS,DDXWD,DDXGC,
     X                        DDAWD(1,1),DDXYZ-DDAWD(2,2),
     X                        DDAWD(1,2)-DDAWD(1,1)+1,
     X                        DDAWD(2,2)-DDAWD(2,1)+1)
        IF (DDXEX.GT.1) THEN
          CALL X$FILL_RECTANGLE(DDXDS,DDXPM,DDXGC,
     X                          DDAWD(1,1),DDXYZ-DDAWD(2,2),
     X                          DDAWD(1,2)-DDAWD(1,1)+1,
     X                          DDAWD(2,2)-DDAWD(2,1)+1)
        END IF
      END IF
      CALL X$FLUSH(DDXDS)
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 DDXDM=DDIN(4)
      DDXPN=0
      DDXPF=0
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
  351 CALL UGXS03
      IF (DDXEX.GT.2) THEN
        CALL X$COPY_AREA(DDXDS,DDXPM,DDXWD,DDXGC,
     X                   0,0,DDXXZ,DDXYZ,0,0)
      END IF
      CALL X$FLUSH(DDXDS)
      DDXDM=0
      GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
  451 CALL UGXS03
      DDXPN=0
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.4)) GO TO 902
      GO TO (461,471,481,491),INT1
C    INQUIRE ABOUT POINTS.
  461 CALL UGXS02(0,DDIN(4),0)
      DDXPF=0
      GO TO 901
C    INQUIRE ABOUT LINES.
  471 CALL UGXS02(1,DDIN(4),DDIN(7))
      DDXPF=1
      GO TO 901
C    INQUIRE ABOUT TEXT.
  481 IF (DDXCT.NE.0) GO TO 902
      IF ((DDIN(8).GE.5).AND.(DDIN(8).LE.355)) GO TO 902
      IF (DDIN(9).NE.2) THEN
        IF (DDIN(7).LT.(DDXFW(1)/2)) GO TO 902
        IF (DDIN(7).GT.(3*DDXFW(DDXZ2)/2)) GO TO 902
      END IF
      CSIZ=1
      DO 467 INT1=2,DDXZ2
        IF (DDIN(7).GT.((DDXFW(INT1-1)+DDXFW(INT1))/2)) CSIZ=INT1
  467 CONTINUE
      CALL UGXS02(2,DDIN(4),0)
      CALL X$SET_FONT(DDXDS,DDXGC,DDXFI(CSIZ))
      DDEX(2)=-DDXFW(CSIZ)/2
      DDEX(3)=-DDXFH(CSIZ)/2
      DDEX(4)=DDXFW(CSIZ)
      DDEX(5)=0
      GO TO 901
C    INQUIRE ABOUT POLYGON FILL.
  491 CALL UGXS02(3,DDIN(4),0)
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.4)) GO TO 902
      GO TO (511,521,531,541),INT1
C    DISPLAY POINTS.
  511 IF (DDXPN.EQ.DDXZ1) CALL UGXS03
      XPNT.X$W_GPNT_X=DDIN(3)
      XPNT.X$W_GPNT_Y=DDXYZ-DDIN(4)
      DDXPN=DDXPN+1
      DDXPT(DDXPN)=XPNT.X$L_GPNT
      GO TO 901
C    DISPLAY LINES.
  521 IF (DDXPN.EQ.DDXZ1) CALL UGXS03
      IF (DDIN(5).EQ.0) THEN
        CALL UGXS03
        DDXPN=0
      END IF
      XPNT.X$W_GPNT_X=DDIN(3)
      XPNT.X$W_GPNT_Y=DDXYZ-DDIN(4)
      DDXPN=DDXPN+1
      DDXPT(DDXPN)=XPNT.X$L_GPNT
      GO TO 901
C    DISPLAY TEXT.
  531 CALL UGXS04(DDST)
      IF (DDXEX.LT.3) THEN
        CALL X$DRAW_STRING(DDXDS,DDXWD,DDXGC,
     X                     DDIN(3),DDXYZ-DDIN(4),DDST)
      END IF
      IF (DDXEX.GT.1) THEN
        CALL X$DRAW_STRING(DDXDS,DDXPM,DDXGC,
     X                     DDIN(3),DDXYZ-DDIN(4),DDST)
      END IF
      GO TO 901
C    DISPLAY POLYGON FILL.
  541 DDXPN=0
      DO 542 INT1=1,DDIN(3)
        XPNT.X$W_GPNT_X=DDIN(2*INT1+2)
        XPNT.X$W_GPNT_Y=DDXYZ-DDIN(2*INT1+3)
        DDXPN=DDXPN+1
        DDXPT(DDXPN)=XPNT.X$L_GPNT
  542 CONTINUE
      IF (DDXEX.LT.3) THEN
        CALL X$FILL_POLYGON(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                      X$C_POLYCOMPLEX,
     X                      X$C_COORD_MODE_ORIGIN)
      END IF
      IF (DDXEX.GT.1) THEN
        CALL X$FILL_POLYGON(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                      X$C_POLYCOMPLEX,
     X                      X$C_COORD_MODE_ORIGIN)
      END IF
      DDXPN=0
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 IF (DDIN(2).NE.1) GO TO 902
      CALL X$BELL(DDXDS,100)
      CALL X$FLUSH(DDXDS)
      GO TO 901
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END
      SUBROUTINE UGXS02(TYPE,COLR,LTYP)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO SET UP THE GRAPHIC CONTEXT FOR A      *
C *  GRAPHIC PRIMITIVE.                                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXS02(TYPE,COLR,LTYP)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    TYPE  THE TYPE OF THE PRIMITIVE.                               *
C *            0 MEANS POINTS,                                        *
C *            1 MEANS LINES,                                         *
C *            2 MEANS TEXT, AND                                      *
C *            3 MEANS POLYGON FILL.                                  *
C *    COLR  THE COLOR OF THE PRIMITIVE.                              *
C *    LTYP  THE LINE TYPE FOR A LINE PRIMITIVE.                      *
C *                                                                   *
C *********************************************************************
C
      INTEGER       TYPE,COLR,LTYP
C
      INCLUDE       '[.include]UGDDXXWS.FOR/NOLIST'
C
      PARAMETER     X$C_LINE_SOLID         = '00000000'X
      PARAMETER     X$C_LINE_ON_OFF_DASH   = '00000001'X
      PARAMETER     X$C_CAP_BUTT           = '00000001'X
      PARAMETER     X$C_JOIN_MITER         = '00000000'X
C
      INTEGER       ACLR,ALTP
      BYTE          DASH(2),DOTS(2),DDSH(4)
C
      DATA          DASH/13,13/
      DATA          DOTS/1,9/
      DATA          DDSH/9,9,1,9/
C
C  SET THE COLOR IN THE GRAPHIC CONTEXT.
      IF (DDXDM.NE.0) THEN
        ACLR=8
      ELSE
        ACLR=COLR
      END IF
      IF (DDXTY.EQ.0) THEN
        IF (ACLR.NE.8) ACLR=1
      END IF
      CALL X$SET_FOREGROUND(DDXDS,DDXGC,DDXCI(ACLR))
C
C  SET THE LINE STRUCTURE IN THE GRAPHIC CONTEXT.
      IF (TYPE.NE.1) THEN
        ALTP=1
      ELSE
        ALTP=LTYP
      END IF
      IF (ALTP.EQ.2) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DASH,2)
      ELSE IF (ALTP.EQ.3) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DOTS,2)
      ELSE IF (ALTP.EQ.4) THEN
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_ON_OFF_DASH,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
        CALL X$SET_DASHES(DDXDS,DDXGC,0,DDSH,4)
      ELSE
        CALL X$SET_LINE_ATTRIBUTES(DDXDS,DDXGC,0,
     X                             X$C_LINE_SOLID,
     X                             X$C_CAP_BUTT,
     X                             X$C_JOIN_MITER)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGXS03
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO WRITE OUT THE ACCUMULATED CONTENTS    *
C *  OF THE POINT/LINE END POINT ARRAY.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXS03                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE       '[.include]UGDDXXWS.FOR/NOLIST'
C
      PARAMETER     X$C_COORD_MODE_ORIGIN  = '00000000'X
C
C  WRITE OUT THE ACCUMULATED POINTS AND RE-INITIALIZE THE POINT
C  ARRAY.
      IF (DDXPF.EQ.0) THEN
C    WRITE OUT POINT DATA.
        IF (DDXPN.GT.0) THEN
          IF (DDXEX.LT.3) THEN
            CALL X$DRAW_POINTS(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                         X$C_COORD_MODE_ORIGIN)
          END IF
          IF (DDXEX.GT.1) THEN
            CALL X$DRAW_POINTS(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                         X$C_COORD_MODE_ORIGIN)
          END IF
        END IF
        DDXPN=0
      ELSE
C    WRITE OUT LINE DATA.
        IF (DDXPN.GT.1) THEN
          IF (DDXEX.LT.3) THEN
            CALL X$DRAW_LINES(DDXDS,DDXWD,DDXGC,DDXPT,DDXPN,
     X                        X$C_COORD_MODE_ORIGIN)
          END IF
          IF (DDXEX.GT.1) THEN
            CALL X$DRAW_LINES(DDXDS,DDXPM,DDXGC,DDXPT,DDXPN,
     X                        X$C_COORD_MODE_ORIGIN)
          END IF
        END IF
        DDXPT(1)=DDXPT(DDXZ1)
        DDXPN=1
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGXS04(STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TRANSLATE CHARACTER STRINGS FROM THE  *
C *  COMPUTER CHARACTER SET TO THE DISPLAY CHARACTER SET.             *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXS04(STRG)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    STRG  THE CHARACTER STRING TO BE TRANSLATED.                   *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) STRG
C
      INTEGER       INT1
      CHARACTER*1   CHR1
C
C  TRANSLATE THE CHARACTER STRING.
      DO 101 INT1=1,LEN(STRG)
        CHR1=STRG(INT1:INT1)
        IF ((CHR1.LT.' ').OR.(CHR1.GT.'~')) THEN
          STRG(INT1:INT1)='@'
        END IF
  101 CONTINUE
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGXS05(ARGU)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS AN ASYNCHRONOUS SYSTEM TRAP (AST) THAT IS     *
C *  ENTERED WHENEVER AN "EXPOSE" EVENT OCCURS.                       *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXS05(ARGU)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    ARGU  A POINTER TO THE AST DATA.                               *
C *                                                                   *
C *********************************************************************
C
      INTEGER*4     ARGU
C
      PARAMETER     X$C_EXPOSE             = '0000000C'X
C
      INTEGER*4     X$PENDING
C
      STRUCTURE     /X$EXPOSE_EVENT/
        INTEGER*4     X$L_EXEV_TYPE
        INTEGER*4     X$L_EXEV_SERIAL
        INTEGER*4     X$L_EXEV_SEND_EVENT
        INTEGER*4     X$A_EXEV_DISPLAY
        INTEGER*4     X$L_EXEV_WINDOW
        INTEGER*4     X$L_EXEV_X
        INTEGER*4     X$L_EXEV_Y
        INTEGER*4     X$L_EXEV_WIDTH
        INTEGER*4     X$L_EXEV_HEIGHT
        INTEGER*4     X$L_EXEV_COUNT
      END STRUCTURE
C
      STRUCTURE     /X$EVENT/
        UNION
        MAP
          INTEGER*4     EVNT_TYPE
        END MAP
        MAP
          RECORD        /X$EXPOSE_EVENT/       EVNT_EXPOSE
        END MAP
        MAP
          INTEGER*4     EVNT_PAD(1:24)
        END MAP
        END UNION
      END STRUCTURE
C
      RECORD        /X$EVENT/              XEVT
C
      EXTERNAL      UGB001
C
      INTEGER*4     ASTD(8),DDXDS,DDXWD,DDXGC,DDXPM,DDXXZ,DDXYZ
      EQUIVALENCE   (DDXDS,ASTD(1)),      (DDXWD,ASTD(2)),
     X              (DDXGC,ASTD(3)),      (DDXPM,ASTD(4)),
     X              (DDXXZ,ASTD(5)),      (DDXYZ,ASTD(6))
C
      INTEGER       B001
C
C  OBTAIN THE AST DATA.
      CALL UGZ004(UGB001,B001)
      CALL UGZ007(B001,1,3,ASTD,1,%LOC(ARGU),1,8)
C
C  PROCESS ANY WAITING EVENTS.
  101 IF (X$PENDING(DDXDS).GT.0) THEN
C    OBTAIN THE NEXT AVAILABLE EVENT.
        CALL X$NEXT_EVENT(DDXDS,XEVT)
        IF (XEVT.EVNT_TYPE.EQ.X$C_EXPOSE) THEN
C    PROCESS AN EXPOSURE EVENT.
          CALL X$COPY_AREA(DDXDS,DDXPM,DDXWD,DDXGC,
     X                     0,0,DDXXZ,DDXYZ,0,0)
          CALL X$FLUSH(DDXDS)
        ELSE
C    SKIP THE EVENT AND TRY AGAIN.
          GO TO 101
        END IF
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
