/*******************  THE UNIFIED GRAPHICS SYSTEM  *******************
 *     SUBROUTINE TO EXECUTE A SUBROUTINE WITH POINTER ARGUMENTS     *
 *                                                                   *
 *  THIS SUBROUTINE MAY BE USED TO EXECUTE A SUBROUTINE WHEN THE     *
 *  ADDRESS OF THE SUBROUTINE AND/OR THE ADDRESSES OF THE            *
 *  SUBROUTINE'S ARGUMENTS ARE KNOWN.  THIS VERSION OF THE           *
 *  SUBROUTINE IS CALLED BY THE DEVICE-INDEPENDENT MODULES.          *
 *                                                                   *
 *  THE CALLING SEQUENCE IS:                                         *
 *    CALL ugz007(SADR,NARG,IARG,ARG1,ARG2,...)                      *
 *                                                                   *
 *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
 *    SADR  THE ADDRESS OF THE SUBROUTINE TO BE CALLED.  THIS        *
 *          ADDRESS MUST HAVE BEEN DEVELOPED BY SUBROUTINE UGZ002    *
 *          OR UGZ004.                                               *
 *    NARG  THE NUMBER OF ARGUMENTS THAT ARE GIVEN BY ADDRESSES.     *
 *    IARG  AN ARRAY CONTAINING THE INDICES OF THE ARGUMENTS GIVEN   *
 *          BY ADDRESSES.                                            *
 *    ARG1  FIRST ACTUAL ARGUMENT.                                   *
 *    ARG2  SECOND ACTUAL ARGUMENT.                                  *
 *    ...   ...                                                      *
 *                                                                   *
 *                          ROBERT C. BEACH                          *
 *                    COMPUTATION RESEARCH GROUP                     *
 *                STANFORD LINEAR ACCELERATOR CENTER                 *
 *                                                                   *
 *********************************************************************/
ugz007(sadr,narg,iarg,arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,
      arg10,arg11,arg12,arg13,arg14,arg15,arg16,arg17,arg18,arg19)
 int *sadr;
 int  *narg,iarg[10],
      *arg0,*arg1,
      *arg2,*arg3,*arg4,*arg5,*arg6,*arg7,*arg8,*arg9,
      *arg10,*arg11,*arg12,*arg13,*arg14,*arg15,*arg16,*arg17,*arg18,*arg19;
{
  void *kludg2();
  int *srg0,*srg1,
      *srg2,*srg3,*srg4,*srg5,*srg6,*srg7,*srg8,*srg9,
      *srg10,*srg11,*srg12,*srg13,*srg14,*srg15,*srg16,*srg17,*srg18,*srg19;
  union {void (*f)() ; void *p;} adr;
  int j;
  adr.p = kludg2(sadr);
  srg0=arg0;
  srg1=arg1;
  srg2=arg2;
  srg3=arg3;
  srg4=arg4;
  srg5=arg5;
  srg6=arg6;
  srg7=arg7;
  srg8=arg8;
  srg9=arg9;
  srg10=arg10;
  srg11=arg11;
  srg12=arg12;
  srg13=arg13;
  srg14=arg14;
  srg15=arg15;
  srg16=arg16;
  srg17=arg17;
  srg18=arg18;
  srg19=arg19;
  for (j=0;j<*narg;j++) {
   switch (iarg[j]-1) {
   case 0:
           srg0 = kludg2(srg0);
           break;
   case 1:
           srg1 = kludg2(srg1);
           break;
   case 2:
           srg2 = kludg2(srg2);
           break;
   case 3:
           srg3 = kludg2(srg3);
           break;
   case 4:
           srg4 = kludg2(srg4);
           break;
   case 5:
           srg5 = kludg2(srg5);
           break;
   case 6:
           srg6 = kludg2(srg6);
           break;
   case 7:
           srg7 = kludg2(srg7);
           break;
   case 8:
           srg8 = kludg2(srg8);
           break;
   case 9:
           srg9 = kludg2(srg9);
           break;
   case 10:
           srg10 = kludg2(srg10);
           break;
   case 11:
           srg11 = kludg2(srg11);
           break;
   case 12:
           srg12 = kludg2(srg12);
           break;
   case 13:
           srg13 = kludg2(srg13);
           break;
   case 14:
           srg14 = kludg2(srg14);
           break;
   case 15:
           srg15 = kludg2(srg15);
           break;
   case 16:
           srg16 = kludg2(srg16);
           break;
   case 17:
           srg17 = kludg2(srg17);
           break;
   case 18:
           srg18 = kludg2(srg18);
           break;
   case 19:
           srg19 = kludg2(srg19);
           break;
 
   printf(" too many srguments in ugz007, %d \n",*narg); exit();
  }
 }
  (*adr.f)(srg0,srg1,srg2,srg3,srg4,srg5,srg6,srg7,srg8,srg9,
         srg10,srg11,srg12,srg13,srg14,srg15,srg16,srg17,srg18,srg19);
}
