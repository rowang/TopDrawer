      SUBROUTINE UGSLCT(OPTN,IDNT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               MAKE AN OPEN DEVICE THE ACTIVE DEVICE               *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SELECT AN OPEN GRAPHIC DEVICE     *
C *  AND MAKE IT THE ACTIVE DEVICE.                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGSLCT(OPTN,IDNT)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    IDNT  THE IDENTIFICATION OF THE GRAPHIC DEVICE THAT IS TO      *
C *          BECOME THE ACTIVE GRAPHIC DEVICE.                        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      INTEGER       IDNT
C
      EXTERNAL      UGB001
C
      INCLUDE        'include/ugmcacbk.for'
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER       B001
      INTEGER       INDX
      INTEGER       TARY(2)
C
      INTEGER       INT1
C
      DATA          TARY/1,3/
C
C  INITIALIZE AND CHECK THE GIVEN IDENTIFICATION.
      IF (IDNT.EQ.0) GO TO 401
      IF (DDAAI.NE.0) THEN
        IF (IDNT.EQ.MCAOI(DDAAI)) GO TO 301
      END IF
      CALL UGZ004(UGB001,B001)
      DO 101 INT1=1,MCAZ1
        IF (IDNT.EQ.MCAOI(INT1)) THEN
          INDX=INT1
          GO TO 201
        END IF
  101 CONTINUE
      GO TO 401
C
C  IF A DEVICE IS ACTIVE, MAKE IT INACTIVE.
  201 IF (DDAAI.NE.0) THEN
        IF (DDAPX.EQ.0) CALL UGZ003(0,DDALX,DDAPX)
        CALL UGZ006(B001,2,TARY,DDAPX,1,DDACX,1,DDALX)
        IF (DDAPA.EQ.0) THEN
          CALL UGZ003(0,DDALG,DDAPA)
          MCAOP(DDAAI)=DDAPA
        END IF
        CALL UGZ006(B001,1,1,DDAPA,1,DDARY,1,DDALG)
        DDAAI=0
      END IF
C
C  MAKE THE SELECTED DEVICE ACTIVE.
      CALL UGZ006(B001,1,3,DDARY,1,MCAOP(INDX),1,DDALG)
      CALL UGZ006(B001,2,TARY,DDACX,1,DDAPX,1,DDALX)
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
  301 UGELV=0
      UGENM='        '
      UGEIX=0
  302 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  401 CALL UGRERR(3,'UGSLCT  ', 1)
      GO TO 302
C
      END

