      SUBROUTINE UGCTOL(OPTN,XCRD,YCRD,TXTP,TXTS,
     X                  NSIZ,XARY,YARY,NCRD,BBTS)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *            CONVERT CHARACTER STRINGS TO LINE SEGMENTS             *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO CONVERT A PRIMARY AND SECONDARY   *
C *  CHARACTER STRING TO LINE SEGMENTS AND THEIR BLANKING BITS.  THE  *
C *  RESULTING DATA MAY BE MODIFIED BEFORE IT IS PASSED ON TO         *
C *  SUBROUTINE UGPLIN.                                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCTOL(OPTN,XCRD,YCRD,TXTP,TXTS,                          *
C *                NSIZ,XARY,YARY,NCRD,BBTS)                          *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    XCRD  THE X COORDINATE OF THE FIRST CHARACTER.                 *
C *    YCRD  THE Y COORDINATE OF THE FIRST CHARACTER.                 *
C *    TXTP  THE PRIMARY CHARACTER STRING.                            *
C *    TXTS  THE SECONDARY CHARACTER STRING.                          *
C *    NSIZ  THE NUMBER OF ENTRIES IN XARY, YARY, AND BBTS.           *
C *    XARY  THE X COORDINATES OF THE LINE END POINTS.                *
C *    YARY  THE Y COORDINATES OF THE LINE END POINTS.                *
C *    NCRD  THE NUMBER OF ENTRIES STORED IN XARY, YARY, AND BBTS.    *
C *    BBTS  THE BLANKING BIT ARRAY.                                  *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      REAL          XCRD,YCRD
      CHARACTER*(*) TXTP,TXTS
      INTEGER       NSIZ
      REAL          XARY(*),YARY(*)
      INTEGER       NCRD
      INTEGER*4     BBTS(*)
C
      EXTERNAL      UGE001,UGE002
C
      INCLUDE        'include/ugmcacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(40)
      INTEGER*4     EXST(5)
      REAL*4        EXSZ,EXAG
      INTEGER*4     EXJF,EXFX,EXLN
      EQUIVALENCE   (EXSZ,EXST(1)),       (EXAG,EXST(2)),
     X              (EXJF,EXST(3)),       (EXFX,EXST(4)),
     X              (EXLN,EXST(5))
C
      INTEGER       NTXT
      INTEGER       E001,E002
      INTEGER       BBIT
      REAL          XVAL,YVAL,SZCH
      INTEGER       FLG1,FLG2
      INTEGER*4     BVAL
      INTEGER*4     BITS(32)
C
      REAL          FLT1,FLT2,FLT3
      INTEGER       INT1,INT2
C
      DATA          INST/7,3,4,1,0,4HSIZE,
     X                     3,5,2,0,4HANGL,4HE   ,
     X                     1,5,3,1,4HRIGH,4HT   ,
     X                     1,6,3,2,4HCENT,4HER  ,
     X                     1,7,4,0,4HFIXS,4HIZE ,
     X                     1,4,5,1,4HLAST,
     X                     1,4,5,2,4HNEXT/
c      DATA          BITS/Z80000000,Z40000000,Z20000000,Z10000000,
c     X                   Z08000000,Z04000000,Z02000000,Z01000000,
c     X                   Z00800000,Z00400000,Z00200000,Z00100000,
c     X                   Z00080000,Z00040000,Z00020000,Z00010000,
c     X                   Z00008000,Z00004000,Z00002000,Z00001000,
c     X                   Z00000800,Z00000400,Z00000200,Z00000100,
c     X                   Z00000080,Z00000040,Z00000020,Z00000010,
c     X                   Z00000008,Z00000004,Z00000002,Z00000001/
      DATA BITS/
     X -2147483648,  1073741824,   536870912,   268435456,
     X   134217728,    67108864,    33554432,    16777216,
     X     8388608,     4194304,     2097152,     1048576,
     X      524288,      262144,      131072,       65536,
     X       32768,       16384,        8192,        4096,
     X        2048,        1024,         512,         256,
     X         128,          64,          32,          16,
     X           8,           4,           2,           1
     X /
C
C  SCAN THE OPTIONS LIST.
      EXSZ=0.015
      EXAG=0.0
      EXJF=0
      EXFX=1
      EXLN=0
      CALL UGOPTN(OPTN,INST,EXST)
C
C  ACTIVATE THE EXTENDED CHARACTER SET IF NECESSARY.
      IF (MCACP.EQ.0) THEN
        CALL UGZ002(0,MCACN,MCACP)
        IF (MCACP.EQ.0) GO TO 303
      END IF
C
C  OBTAIN THE INDIVIDUAL STROKES.
      NTXT=MIN(LEN(TXTP),LEN(TXTS))
      IF (NTXT.LT.1) GO TO 301
      IF (NTXT.GT.1024) GO TO 301
      CALL UGZ004(UGE001,E001)
      CALL UGZ004(UGE002,E002)
      NCRD=0
      FLG1=EXLN
      FLG2=EXFX
      XVAL=XCRD
      YVAL=YCRD
      IF (EXJF.NE.0) THEN
        CALL UGZ006(E001,1,10,1,FLG2,1,1,0.0,0.0,EXSZ,
     X              EXAG,1.0,MCACP,INT1)
        IF (INT1.NE.0) GO TO 303
        CALL UGZ006(E002,1,4,TXTP,TXTS,NTXT,
     X              MCACP,INT1,FLT1,FLT2,FLT3)
        XVAL=XVAL-0.5*FLT1
        YVAL=YVAL-0.5*FLT2
        IF (EXJF.NE.2) THEN
          XVAL=XVAL-0.5*FLT1
          YVAL=YVAL-0.5*FLT2
        END IF
      END IF
      CALL UGZ006(E001,1,10,FLG1,FLG2,1,1,XVAL,YVAL,EXSZ,
     X            EXAG,1.0,MCACP,INT1)
      IF (INT1.NE.0) GO TO 303
  101 CALL UGZ006(E002,1,4,TXTP,TXTS,NTXT,
     X            MCACP,BBIT,XVAL,YVAL,SZCH)
      IF (FLG1.EQ.0) THEN
        IF (BBIT.EQ.-1) GO TO 201
        IF (NCRD.GE.NSIZ) GO TO 302
        NCRD=NCRD+1
        XARY(NCRD)=XVAL
        YARY(NCRD)=YVAL
        IF (NCRD.GT.1) THEN
          INT1=(NCRD+30)/32
          INT2=NCRD-32*INT1+31
          BVAL=IAND(BBTS(INT1),NOT(BITS(INT2)))
          IF (BBIT.NE.0) BVAL=IOR(BVAL,BITS(INT2))
          BBTS(INT1)=BVAL
        END IF
        GO TO 101
      END IF
      XARY(1)=XVAL
      YARY(1)=YVAL
      XARY(2)=SZCH
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
  201 UGELV=0
      UGENM='        '
      UGEIX=0
  202 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  301 CALL UGRERR(2,'UGCTOL  ', 1)
      GO TO 202
  302 CALL UGRERR(2,'UGCTOL  ', 2)
      GO TO 202
  303 CALL UGRERR(3,'UGCTOL  ',14)
      GO TO 202
C
      END

