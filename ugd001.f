      SUBROUTINE UGD001(FGLS,XCMU,YCMU)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                       LINE STRUCTURE MODULE                       *
C *                                                                   *
C *  THIS IS THE FIRST OF THREE SUBROUTINES THAT MAY BE USED TO       *
C *  GENERATE LINE STRUCTURE.  SOLID LINES MAY BE BROKEN DOWN INTO:   *
C *  (1) "DASHED" LINES, (2) "DOTTED" LINES, OR (3) "DOTDASH" LINES.  *
C *  SUBROUTINE UGD001 IS USED TO INITIALIZE THE PROCESS FOR A NEW    *
C *  SERIES OF LINE SEGMENTS, SUBROUTINE UGD002 IS USED TO SUPPLY     *
C *  AN END POINT TO THE LINE STRUCTURE GENERATING MODULES, AND       *
C *  SUBROUTINE UGD003 IS USED TO RETRIEVE A POINT OR LINE SEGMENT    *
C *  END POINT FROM THE LINE STRUCTURE GENERATING MODULES.  THE       *
C *  NORMAL SEQUENCE OF CALLS IS THE FOLLOWING: (1) UGD001 IS CALLED  *
C *  TO INITIALIZE PROCESSING, (2) UGD002 IS CALLED TO SUPPLY AN END  *
C *  POINT TO THE MODULES, (3) UGD003 IS CALLED REPEATEDLY TO         *
C *  RETRIEVE POINTS OR END POINTS OF LINES UNTIL IT SIGNALS THAT NO  *
C *  MORE DATA IS AVAILABLE, AND (4) STEP 2 IS REPEATED UNTIL NO      *
C *  MORE INPUT IS AVAILABLE.                                         *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO INITIALIZE LINE STRUCTURE         *
C *  GENERATION.                                                      *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGD001(FGLS,XCMU,YCMU)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FGLS  LINE STRUCTURE FLAG: 1 MEANS "DASHED"; 2 MEANS           *
C *          "DOTTED"; AND 3 MEANS "DOTDASH".                         *
C *    XCMU  CENTIMETERS PER UNIT DISTANCE IN THE X DIRECTION.        *
C *    YCMU  CENTIMETERS PER UNIT DISTANCE IN THE Y DIRECTION.        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FGLS
      REAL          XCMU,YCMU
C
      INCLUDE        'include/ugd000.for'
C
C  INITIALIZE THE LINE STRUCTURE MODULE.
      IFLS=FGLS
      CMUX=XCMU
      CMUY=YCMU
      FGAV=0
      IF (IFLS.EQ.1) THEN
        SZDS=0.333333333
        SZBK=0.333333333
      ELSE IF (IFLS.EQ.2) THEN
        SZBK=0.25
      ELSE
        SZDS=0.25
        SZBK=0.25
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGD002(BBIT,XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                       LINE STRUCTURE MODULE                       *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SUPPLY A POINT TO THE LINE        *
C *  STRUCTURE MODULE.                                                *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGD002(BBIT,XCRD,YCRD)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT: 0 MEANS MOVE WITHOUT DRAWING AND 1     *
C *          MEANS DRAW.                                              *
C *    XCRD  THE X COORDINATE OF A LINE END POINT.                    *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT.                    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD
C
      INCLUDE        'include/ugd000.for'
C
      REAL          FLT1,FLT2,FLT3,FLT4,FLT5
C
C  SUPPLY AN END POINT TO THE LINE STRUCTURE MODULE.
      IF (BBIT.EQ.0) THEN
        PNT2(1)=XCRD
        PNT2(2)=YCRD
        TVL2=0.0
        TVAL=0.0
        FGLO=0
        FGAV=1
      ELSE
        FLT1=XCRD-PNT2(1)
        FLT2=YCRD-PNT2(2)
        FLT3=FLT1*CMUX
        FLT4=FLT2*CMUY
        FLT5=SQRT(FLT3*FLT3+FLT4*FLT4)
        IF (FLT5.GE.0.001) THEN
          PNT1(1)=PNT2(1)
          PNT1(2)=PNT2(2)
          TVL1=TVL2
          PNT2(1)=XCRD
          PNT2(2)=YCRD
          TVL2=TVL1+FLT5
          DELX=FLT1/FLT5
          DELY=FLT2/FLT5
          FGAV=1
        END IF
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGD003(BBIT,XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                       LINE STRUCTURE MODULE                       *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO RETRIEVE A POINT FROM THE LINE    *
C *  STRUCTURE MODULE.                                                *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGD003(BBIT,XCRD,YCRD)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT OR TERMINATION FLAG: 0 MEANS MOVE       *
C *          WITHOUT DRAWING, 1 MEANS DRAW, 2 MEANS DRAW A POINT,     *
C *          AND -1 MEANS NO MORE DATA IS AVAILABLE.                  *
C *    XCRD  THE X COORDINATE OF A LINE END POINT OR A POINT.         *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT OR A POINT.         *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD
C
      INCLUDE        'include/ugd000.for'
C
      INTEGER       INT1
C
C  CHECK TO SEE IF ANY DATA IS AVAILABLE.
      IF (FGAV.EQ.0) THEN
        BBIT=-1
        GO TO 201
      END IF
C
C  PROCESS DATA ACCORDING TO LINE STRUCTURE AND LAST OPERATION.
      INT1=3*FGLO+IFLS
      GO TO (101,108,110,
     X       103,202,111,
     X       106,202,112,
     X       202,109,113), INT1
C  DASHED LINE, BLANK VECTOR SUPPLIED.
  101 FGLO=1
      BBIT=0
  102 FGAV=0
      XCRD=PNT2(1)
      YCRD=PNT2(2)
      GO TO 201
C  DASHED LINE, DREW BLANK.
  103 IF ((TVAL+SZDS).GT.TVL2) GO TO 105
      TVAL=TVAL+SZDS
      FGLO=2
      BBIT=1
  104 XCRD=DELX*(TVAL-TVL1)+PNT1(1)
      YCRD=DELY*(TVAL-TVL1)+PNT1(2)
      GO TO 201
  105 BBIT=1
      GO TO 102
C  DASHED LINE, DREW DASH.
  106 IF ((TVAL+SZBK).GT.TVL2) GO TO 107
      TVAL=TVAL+SZBK
      FGLO=1
      BBIT=0
      GO TO 104
  107 FGAV=0
      BBIT=-1
      GO TO 201
C  DOTTED LINE, BLANK VECTOR SUPPLIED.
  108 FGLO=3
      BBIT=2
      GO TO 102
C  DOTTED LINE, DREW POINT.
  109 IF ((TVAL+SZBK).GT.TVL2) GO TO 107
      TVAL=TVAL+SZBK
      BBIT=2
      GO TO 104
C  DOTDASH LINE, BLANK VECTOR SUPPLIED.
  110 FGLO=1
      BBIT=0
      GO TO 102
C  DOTDASH LINE, DREW BLANK.
  111 IF ((TVAL+SZDS).GT.TVL2) GO TO 105
      TVAL=TVAL+SZDS
      FGLO=2
      BBIT=1
      GO TO 104
C  DOTDASH LINE, DREW DASH.
  112 IF ((TVAL+SZBK).GT.TVL2) GO TO 107
      TVAL=TVAL+SZBK
      FGLO=3
      BBIT=2
      GO TO 104
C  DOTDASH LINE, DREW POINT.
  113 IF ((TVAL+SZBK).GT.TVL2) GO TO 107
      TVAL=TVAL+SZBK
      FGLO=1
      BBIT=0
      GO TO 104
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C  TERMINATE ON IMPOSSIBLE TRANSFER.
  202 CALL UGZ001
      GO TO 201
C
      END

