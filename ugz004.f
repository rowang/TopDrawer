c/*******************  THE UNIFIED GRAPHICS SYSTEM  *******************
c *          SUBROUTINE TO DETERMINE A SUBROUTINE'S ADDRESS           *
c *                                                                   *
c *  THIS SUBROUTINE MAY BE USED TO DETERMINE THE ADDRESS OF A        *
c *  SUBROUTINE WITHIN THE CURRENT LOAD MODULE.                       *
c *                                                                   *
c *  THE CALLING SEQUENCE IS:                                         *
c *    CALL UGZ004(SUBR,SADR)                                         *
c *                                                                   *
c *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
c *    SUBR  THE SUBROUTINE WHOSE ADDRESS IS NEEDED.                  *
c *    SADR  THE ADDRESS OF THE SUBROUTINE.                           *
c *                                                                   *
c *                          ROBERT C. BEACH                          *
c *                    COMPUTATION RESEARCH GROUP                     *
c *                STANFORD LINEAR ACCELERATOR CENTER                 *
c *                                                                   *
c *********************************************************************/
      subroutine UGZ004(fun,addr)
c      external fun
      integer fun,addr
      addr=kludg1(fun)
      end

