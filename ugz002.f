      SUBROUTINE UGZ002(FLAG,NAME,PNTR)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *         SUBROUTINE TO ACTIVATE/DE-ACTIVATE A NAMED MODULE         *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO ACTIVATE OR DE-ACTIVATE A NAMED   *
C *  MODULE.  WHEN A MODULE IS ACTIVATED, THE ADDRESS OF THE MODULE   *
C *  IS RETURNED.  IF THE MODULE CANNOT BE FOUND, A ZERO IS           *
C *  RETURNED.  WHEN A MODULE IS DE-ACTIVATED, BOTH ITS NAME AND ITS  *
C *  ADDRESS SHOULD BE GIVEN.                                         *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGZ002(FLAG,NAME,PNTR)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  THE OPERATION INDICATOR (0 MEANS ACTIVATE AND 1 MEANS    *
C *          DE-ACTIVATE).                                            *
C *    NAME  THE NAME OF THE MODULE TO BE ACTIVATED.  THIS ARGUMENT   *
C *          MUST BE A CHARACTER STRING OF EIGHT CHARACTERS.          *
C *    PNTR  THE ADDRESS OF THE MODULE.                               *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      CHARACTER*8   NAME
      INTEGER       PNTR
C
C  NUMBER OF MODULES THAT MAY BE ACTIVATED.
      INTEGER       NMOD
      PARAMETER     (NMOD=45)
C
      SAVE          /UGA013/
      COMMON        /UGA013/
     X              SMPLX
      INTEGER*2     SMPLX
C
      SAVE          /UGA014/
      COMMON        /UGA014/
     X              DUPLX
      INTEGER*2     DUPLX
C
      EXTERNAL      UGCW01,
     X              UGGS01,
     X              UGGD01,
     X              UGGI01,
     X              UGVS01,
     X              UGVI01,
     X              UGGR01,
     X              UGIN01,
     X              UGIX01,
     X              UGMT01,
     X              UGPR01,
     X              UGPX01,
     X              UGSA01,
     X              UGSB01,
     X              UGSC01,
     X              UGSD01,
     X              UGSE01,
     X              UGSX01,
     X              UGTA01,
     X              UGTS01,
     X              UGTD01,
     X              UGTX01,
     X              UGXA01,
     X              UGXB01,
     X              UGXC01,
     X              UGUS01,
     X              UGUD01,
     X              UGUX01,
     X              UGWA01,
     X              UGWB01,
     X              UGWC01,
     X              UGWD01,
     X              UGWE01,
     X              UGWZ01,
     X              UGVF01,
     X              UGXS01,
     X              UGXI01,
     X              UGPU01,
     X              UGPL01,
     X              UGPS01,
     X              UGPM01,
     X              UGPI01,
     X              UGZZ01
C
      SAVE          MODF,MODN,MODP
      LOGICAL       MODF
      CHARACTER*8   MODN(NMOD)
      INTEGER       MODP(NMOD)
C
      INTEGER       INT1
C
      DATA          MODF/.TRUE./
      DATA          MODN/'UGA013  ',
     X                   'UGA014  ',
     X                   'UGCW01  ',
     X                   'UGGS01  ',
     X                   'UGGD01  ',
     X                   'UGGI01  ',
     X                   'UGVS01  ',
     X                   'UGVI01  ',
     X                   'UGGR01  ',
     X                   'UGIN01  ',
     X                   'UGIX01  ',
     X                   'UGMT01  ',
     X                   'UGPR01  ',
     X                   'UGPX01  ',
     X                   'UGSA01  ',
     X                   'UGSB01  ',
     X                   'UGSC01  ',
     X                   'UGSD01  ',
     X                   'UGSE01  ',
     X                   'UGSX01  ',
     X                   'UGTA01  ',
     X                   'UGTS01  ',
     X                   'UGTD01  ',
     X                   'UGTX01  ',
     X                   'UGXA01  ',
     X                   'UGXB01  ',
     X                   'UGXC01  ',
     X                   'UGUS01  ',
     X                   'UGUD01  ',
     X                   'UGUX01  ',
     X                   'UGWA01  ',
     X                   'UGWB01  ',
     X                   'UGWC01  ',
     X                   'UGWD01  ',
     X                   'UGWE01  ',
     X                   'UGWZ01  ',
     X                   'UGVF01  ',
     X                   'UGXS01  ',
     X                   'UGXI01  ',
     X                   'UGPU01  ',
     X                   'UGPL01  ',
     X                   'UGPS01  ',
     X                   'UGPM01  ',
     X                   'UGPI01  ',
     X                   'UGZZ01  '/
C
C  INITIALIZE THE MODULE DATA ON THE FIRST ENTRY.
      IF (MODF) THEN
        MODF=.FALSE.
        MODP( 1)=KLUDG1(SMPLX)
        MODP( 2)=KLUDG1(DUPLX)
        MODP( 3)=KLUDG1(UGCW01)
        MODP( 4)=KLUDG1(UGGS01)
        MODP( 5)=KLUDG1(UGGD01)
        MODP( 6)=KLUDG1(UGGI01)
        MODP( 7)=KLUDG1(UGVS01)
        MODP( 8)=KLUDG1(UGVI01)
        MODP( 9)=KLUDG1(UGGR01)
        MODP(10)=KLUDG1(UGIN01)
        MODP(11)=KLUDG1(UGIX01)
        MODP(12)=KLUDG1(UGMT01)
        MODP(13)=KLUDG1(UGPR01)
        MODP(14)=KLUDG1(UGPX01)
        MODP(15)=KLUDG1(UGSA01)
        MODP(16)=KLUDG1(UGSB01)
        MODP(17)=KLUDG1(UGSC01)
        MODP(18)=KLUDG1(UGSD01)
        MODP(19)=KLUDG1(UGSE01)
        MODP(20)=KLUDG1(UGSX01)
        MODP(21)=KLUDG1(UGTA01)
        MODP(22)=KLUDG1(UGTS01)
        MODP(23)=KLUDG1(UGTD01)
        MODP(24)=KLUDG1(UGTX01)
        MODP(25)=KLUDG1(UGXA01)
        MODP(26)=KLUDG1(UGXB01)
        MODP(27)=KLUDG1(UGXC01)
        MODP(28)=KLUDG1(UGUS01)
        MODP(29)=KLUDG1(UGUD01)
        MODP(30)=KLUDG1(UGUX01)
        MODP(31)=KLUDG1(UGWA01)
        MODP(32)=KLUDG1(UGWB01)
        MODP(33)=KLUDG1(UGWC01)
        MODP(34)=KLUDG1(UGWD01)
        MODP(35)=KLUDG1(UGWE01)
        MODP(36)=KLUDG1(UGWZ01)
        MODP(37)=KLUDG1(UGVF01)
        MODP(38)=KLUDG1(UGXS01)
        MODP(39)=KLUDG1(UGXI01)
        MODP(40)=KLUDG1(UGPU01)
        MODP(41)=KLUDG1(UGPL01)
        MODP(42)=KLUDG1(UGPS01)
        MODP(43)=KLUDG1(UGPM01)
        MODP(44)=KLUDG1(UGPI01)
        MODP(45)=KLUDG1(UGZZ01)
      END IF
C
C  IF THIS IS AN ACTIVATION REQUEST, OBTAIN THE ADDRESS.
      IF (FLAG.EQ.0) THEN
        DO 101 INT1=1,NMOD
          IF (NAME.EQ.MODN(INT1)) THEN
            PNTR=MODP(INT1)
            GO TO 201
          END IF
  101   CONTINUE
        PNTR=0
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C
      END

