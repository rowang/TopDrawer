      SUBROUTINE UGOPEN(OPTN,IDNT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                       OPEN A GRAPHIC DEVICE                       *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO OPEN A GRAPHIC DEVICE AND MAKE    *
C *  IT READY FOR USE.  A GRAPHIC DEVICE MUST BE OPENED BEFORE ANY    *
C *  USE CAN BE MADE OF IT.  OPENING A GRAPHIC DEVICE MAKES IT THE    *
C *  ACTIVE DEVICE.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGOPEN(OPTN,IDNT)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    IDNT  THE IDENTIFICATION OF THE GRAPHIC DEVICE.                *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      INTEGER       IDNT
C
C  NUMBER OF SUPPORTED GRAPHIC DEVICES.
      INTEGER       NSGD
      PARAMETER     (NSGD=43)
C
      EXTERNAL      UGB001
C
      INCLUDE        'include/ugmcacbk.for'
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(6*NSGD+1)
      INTEGER*4     EXST(1),EXIX
      EQUIVALENCE   (EXIX,EXST(1))
C
      INTEGER       B001
      INTEGER       INDX
      CHARACTER*8   NAMS(NSGD)
      INTEGER       TARY(2)
      INTEGER       DDIN(2),DDEX(1)
C
      REAL          FLT1,FLT2
      INTEGER       INT1,INT2
C
      DATA          INST/NSGD,1,7,1, 1,4HSDDX,4HCOW ,
     X                        1,7,1, 2,4HSEQG,4HIGI ,
     X                        1,7,1, 3,4HSDDG,4HIGI ,
     X                        1,7,1, 4,4HDECG,4HIGI ,
     X                        1,7,1, 5,4HSDDV,4HST2 ,
     X                        1,7,1, 6,4HDECV,4HST2 ,
     X                        1,7,1, 7,4HSDDG,4HRIN ,
     X                        1,7,1, 8,4HIMGN,4H300 ,
     X                        1,7,1, 9,4HIMGN,4HIBM ,
     X                        1,7,1,10,4HSDDM,4HETH ,
     X                        1,7,1,11,4HPOST,4HSCR ,
     X                        1,7,1,12,4HPRNT,4HRNX ,
     X                        1,7,1,13,4HSEQS,4HEKO ,
     X                        1,7,1,14,4HSDDS,4HKRS ,
     X                        1,7,1,15,4HSEIK,4HORS ,
     X                        1,7,1,16,4HSDDS,4HKRF ,
     X                        1,7,1,17,4HSEIK,4HORF ,
     X                        1,7,1,18,4HSDDS,4HXSS ,
     X                        1,7,1,19,4HTALA,4HRIS ,
     X                        1,7,1,20,4HSEQ4,4H010 ,
     X                        1,7,1,21,4HSDD4,4H010 ,
     X                        1,7,1,22,4HTEK4,4H010 ,
     X                        1,7,1,23,4HSEQT,4HKEM ,
     X                        1,7,1,24,4HSDDT,4HKEM ,
     X                        1,7,1,25,4HTEKE,4HMUL ,
     X                        1,7,1,26,4HSEQ4,4H027 ,
     X                        1,7,1,27,4HSDD4,4H027 ,
     X                        1,7,1,28,4HTEK4,4H027 ,
     X                        1,7,1,29,4HSEQ4,4H105 ,
     X                        1,7,1,30,4HSDD4,4H105 ,
     X                        1,7,1,31,4HTEK4,4H105 ,
     X                        1,7,1,32,4HSDD4,4H207 ,
     X                        1,7,1,33,4HTEK4,4H207 ,
     X                        1,7,1,34,4HTEK4,4H510 ,
     X                        1,7,1,35,4HVEP1,4H2FF ,
     X                        1,7,1,36,4HSDDX,4HWDO ,
     X                        1,7,1,37,4HXWIN,4HDOW ,
     X                        1,7,1,38,4HPDEV,4HUGS ,
     X                        1,7,1,39,4HPDEV,4HLIN ,
     X                        1,7,1,40,4HPDEV,4HSVR ,
     X                        1,7,1,41,4HPDEV,4HMVR ,
     X                        1,7,1,42,4HGENW,4HKST ,
     X                        1,7,1,43,4HTEST,4HDEV /
      DATA          NAMS/'UGCW01  ',
     X                   'UGGS01  ',
     X                   'UGGD01  ',
     X                   'UGGI01  ',
     X                   'UGVS01  ',
     X                   'UGVI01  ',
     X                   'UGGR01  ',
     X                   'UGIN01  ',
     X                   'UGIX01  ',
     X                   'UGMT01  ',
     X                   'UGPR01  ',
     X                   'UGPX01  ',
     X                   'UGSA01  ',
     X                   'UGSB01  ',
     X                   'UGSC01  ',
     X                   'UGSD01  ',
     X                   'UGSE01  ',
     X                   'UGSX01  ',
     X                   'UGTA01  ',
     X                   'UGTS01  ',
     X                   'UGTD01  ',
     X                   'UGTX01  ',
     X                   'UGXA01  ',
     X                   'UGXB01  ',
     X                   'UGXC01  ',
     X                   'UGUS01  ',
     X                   'UGUD01  ',
     X                   'UGUX01  ',
     X                   'UGWA01  ',
     X                   'UGWB01  ',
     X                   'UGWC01  ',
     X                   'UGWD01  ',
     X                   'UGWE01  ',
     X                   'UGWZ01  ',
     X                   'UGVF01  ',
     X                   'UGXS01  ',
     X                   'UGXI01  ',
     X                   'UGPU01  ',
     X                   'UGPL01  ',
     X                   'UGPS01  ',
     X                   'UGPM01  ',
     X                   'UGPI01  ',
     X                   'UGZZ01  '/
      DATA          TARY/1,3/
C
C  SCAN THE OPTIONS LIST.
      EXIX=0
      CALL UGOPTN(OPTN,INST,EXST)
C
C  CHECK FOR INPUT ERRORS.
      IF (EXIX.EQ.0) GO TO 501
      IF (IDNT.EQ.0) GO TO 502
      INDX=0
      DO 101 INT1=MCAZ1,1,-1
        IF (MCAOI(INT1).EQ.IDNT) GO TO 503
        IF (MCAOI(INT1).EQ.0) INDX=INT1
  101 CONTINUE
      IF (INDX.EQ.0) GO TO 504
C
C  IF A DEVICE IS ACTIVE, MAKE IT INACTIVE.
      IF (DDAAI.NE.0) THEN
        CALL UGZ004(UGB001,B001)
        IF (DDAPX.EQ.0) CALL UGZ003(0,DDALX,DDAPX)
        CALL UGZ006(B001,2,TARY,DDAPX,1,DDACX,1,DDALX)
        IF (DDAPA.EQ.0) THEN
          CALL UGZ003(0,DDALG,DDAPA)
          MCAOP(DDAAI)=DDAPA
        END IF
        CALL UGZ006(B001,1,1,DDAPA,1,DDARY,1,DDALG)
        DDAAI=0
      END IF
C
C  ACTIVATE THE DEVICE DEPENDENT CODE.
      CALL UGZ002(0,NAMS(EXIX),DDAAC)
      IF (DDAAC.EQ.0) GO TO 505
C
C  PRE-INITIALIZE THE DEVICE DEPENDENT AREA.
      DDAPA=0
      DDAPX=0
      DDAAN=NAMS(EXIX)
      DDAIL=1
      DDADM=1
      DDADF=2
      DO 201 INT1=1,DDAZ1
        DDAIC(INT1)=0
        DDABC(INT1)=0
  201 CONTINUE
      DDAKX=0
      DDAKY=0
      DO 202 INT1=1,DDAZ2
        DDAKS(INT1:INT1)=' '
  202 CONTINUE
      DDAKN=32
      DDAKF=0
      DO 203 INT1=1,DDAZ3
        DDABF(INT1)=0
  203 CONTINUE
      DDASL=(2**30)/128
      DDAST=25
      DDASN=32
      DDABE(1,1)=0
      DDABE(2,1)=0
      DDABE(1,2)=0
      DDABE(2,2)=0
      DDADS(1,1)=0.0
      DDADS(2,1)=0.0
      DDADS(1,2)=1.0
      DDADS(2,2)=1.0
C
C  ENTER DEVICE-DEPENDENT CODE.
      DDIN(1)=1
      CALL UGZ006(DDAAC,0,0,DDIN,OPTN,DDEX)
      IF (DDEX(1).LT.0) GO TO 505
      IF (DDEX(1).NE.0) GO TO 506
C
C  FINISH INITIALIZATION OF THE DEVICE DEPENDENT AREA.
      DDAAI=INDX
      FLT1=DDABX*REAL(DDABD(1,2)-DDABD(1,1))
      FLT2=DDABY*REAL(DDABD(2,2)-DDABD(2,1))
      IF (FLT2.LE.FLT1) THEN
        INT1=NINT((0.5*(FLT1-FLT2)/DDABX))
        DDADD(1,1)=DDABD(1,1)+INT1
        DDADD(2,1)=DDABD(2,1)
        DDADD(1,2)=DDABD(1,2)-INT1
        DDADD(2,2)=DDABD(2,2)
      ELSE
        INT1=NINT((0.5*(FLT2-FLT1)/DDABY))
        DDADD(1,1)=DDABD(1,1)
        DDADD(2,1)=DDABD(2,1)+INT1
        DDADD(1,2)=DDABD(1,2)
        DDADD(2,2)=DDABD(2,2)-INT1
      END IF
      DDADX=DDABX*REAL(DDADD(1,2)-DDADD(1,1))
      DDADY=DDADX
      DDADA=1.0
      DO 302 INT1=1,2
        DO 301 INT2=1,2
          DDAWA(INT1,INT2)=DDADS(INT1,INT2)
          DDAWS(INT1,INT2)=DDADS(INT1,INT2)
          DDAWD(INT1,INT2)=DDADD(INT1,INT2)
          DDA3V(INT1,INT2)=DDADS(INT1,INT2)
  301   CONTINUE
  302 CONTINUE
      DDAWX=DDADX
      DDAWY=DDADY
      CALL UGB005
      DDA3W(1,1)=-4.5
      DDA3W(2,1)=-4.5
      DDA3W(3,1)=-4.5
      DDA3W(1,2)=5.5
      DDA3W(2,2)=5.5
      DDA3W(3,2)=5.5
      CALL UGB010
      CALL UGB011(INT1)
      DDAFW=0
C
C  SAVE DEVICE POINTERS IN THE MAIN COMMUNICATION AREA.
      MCAOI(INDX)=IDNT
      MCAOP(INDX)=0
C
C  CLEAR THE NEWLY ACTIVE DEVICE.
      DDIN(1)=3
      DDIN(2)=0
      CALL UGZ006(DDAAC,0,0,DDIN,'        ',DDEX)
C
C  SEND THE PROJECTION PARAMETERS TO A THREE-DIMENSIONAL DEVICE.
      IF (DDADF.EQ.3) THEN
        DDIN(1)=15
        DDIN(2)=1
        CALL UGZ006(DDAAC,0,0,DDIN,'        ',DDEX)
      END IF
C
C  CHECK FOR MULTIPLE FULLY INTERACTIVE GRAPHIC DEVICES.
      IF (DDAIL.EQ.3) THEN
        MCAIC=MCAIC+1
        IF (MCAIC.GT.1) GO TO 507
      END IF
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
      UGELV=0
      UGENM='        '
      UGEIX=0
  401 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  501 CALL UGRERR(3,'UGOPEN  ', 1)
      GO TO 401
  502 CALL UGRERR(3,'UGOPEN  ', 2)
      GO TO 401
  503 CALL UGRERR(3,'UGOPEN  ', 3)
      GO TO 401
  504 CALL UGRERR(3,'UGOPEN  ', 4)
      GO TO 401
  505 CALL UGRERR(3,'UGOPEN  ', 5)
      GO TO 401
  506 CALL UGRERR(3,'UGOPEN  ', 6)
      GO TO 401
  507 CALL UGRERR(2,'UGOPEN  ', 7)
      GO TO 401
C
      END

