      SUBROUTINE UGINFO(OPTN,STRG,IARY,XARY)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                OBTAIN INFORMATION ABOUT THE SYSTEM                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO DETERMINE THE STATUS OF THE       *
C *  UNIFIED GRAPHICS SYSTEM AND MANY OF THE PROPERTIES OF THE        *
C *  ACTIVE GRAPHIC DEVICE.                                           *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGINFO(OPTN,STRG,IARY,XARY)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    STRG  A CHARACTER STRING FOR THE ANSWERS.                      *
C *    IARY  AN INTEGER ARRAY FOR THE ANSWERS.                        *
C *    XARY  A FLOATING POINT ARRAY FOR THE ANSWERS.                  *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN,STRG
      INTEGER       IARY(*)
      REAL          XARY(*)
C
      INCLUDE        'include/ugmcacbk.for'
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(70)
      INTEGER*4     EXST(11),
     X              EXOD,EXAD,
     X              EXAT,EXIL,EXDM,EXDF,
     X              EXIC,EXEC,
     X              EXBE,
     X              EXDX,EXWX
      EQUIVALENCE   (EXOD,EXST( 1)),      (EXAD,EXST( 2)),
     X              (EXAT,EXST( 3)),      (EXIL,EXST( 4)),
     X              (EXDM,EXST( 5)),      (EXDF,EXST( 6)),
     X              (EXIC,EXST( 7)),      (EXEC,EXST( 8)),
     X              (EXBE,EXST( 9)),      (EXDX,EXST(10)),
     X              (EXWX,EXST(11))
C
      INTEGER       INT1
C
      DATA          INST/11,1, 7, 1,1,4HOPEN,4HDEV ,
     X                      1, 6, 2,1,4HACTD,4HEV  ,
     X                      1, 7, 3,1,4HDEVT,4HYPE ,
     X                      1, 6, 4,1,4HILEV,4HEL  ,
     X                      1, 7, 5,1,4HDMED,4HIUM ,
     X                      1, 9, 6,1,4HDIME,4HNSIO,4HN   ,
     X                      1, 8, 7,1,4HCONT,4HROLS,
     X                      1, 9, 8,1,4HECON,4HTROL,4HS   ,
     X                      1,10, 9,1,4HEXTE,4HNSIO,4HNS  ,
     X                      1, 8,10,1,4HDSPC,4HSIZE,
     X                      1, 8,11,1,4HWDOW,4HSIZE/
C
C  SCAN THE OPTIONS LIST.
      EXOD=0
      EXAD=0
      EXAT=0
      EXIL=0
      EXDM=0
      EXDF=0
      EXIC=0
      EXEC=0
      EXBE=0
      EXDX=0
      EXWX=0
      CALL UGOPTN(OPTN,INST,EXST)
C
C  PROCESS REQUESTS THAT DO NOT REQUIRE AN ACTIVE DEVICE.
C  PROCESS "OPENDEV" IF REQUESTED.
      IF (EXOD.NE.0) THEN
        IARY(1)=0
        DO 101 INT1=1,MCAZ1
          IF (MCAOI(INT1).NE.0) THEN
            IARY(1)=IARY(1)+1
            IARY(IARY(1)+1)=MCAOI(INT1)
          END IF
  101   CONTINUE
      END IF
C  PROCESS "ACTDEV" IF REQUESTED.
      IF (EXAD.NE.0) THEN
        IF (DDAAI.EQ.0) THEN
          IARY(1)=0
        ELSE
          IARY(1)=MCAOI(DDAAI)
        END IF
      END IF
C
C  PROCESS REQUESTS THAT REQUIRE AN ACTIVE DEVICE.
C  PROCESS "DEVTYPE" IF REQUESTED.
      IF (EXAT.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        STRG(1:8)=DDAAT
      END IF
C  PROCESS "ILEVEL" IF REQUESTED.
      IF (EXIL.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        IARY(1)=DDAIL
      END IF
C  PROCESS "DMEDIUM" IF REQUESTED.
      IF (EXDM.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        IARY(2)=DDADM
      END IF
C  PROCESS "DIMENSION" IF REQUESTED.
      IF (EXDF.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        IARY(3)=DDADF
      END IF
C  PROCESS "CONTROLS" IF REQUESTED.
      IF (EXIC.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        DO 201 INT1=1,DDAZ1
          IARY(INT1)=DDAIC(INT1)
  201   CONTINUE
      END IF
C  PROCESS "ECONTROLS" IF REQUESTED.
      IF (EXEC.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        DO 202 INT1=1,DDAZ1
          IARY(INT1)=DDABC(INT1)
  202   CONTINUE
      END IF
C  PROCESS "EXTENSIONS" IF REQUESTED.
      IF (EXBE.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        IARY(1)=DDABE(1,1)
        IARY(2)=DDABE(2,1)
        IARY(3)=DDABE(1,2)
        IARY(4)=DDABE(2,2)
      END IF
C  PROCESS "DSPCSIZE" IF REQUESTED.
      IF (EXDX.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        XARY(1)=DDADX
        XARY(2)=DDADY
      END IF
C  PROCESS "WDOWSIZE" IF REQUESTED.
      IF (EXWX.NE.0) THEN
        IF (DDAAI.EQ.0) GO TO 401
        XARY(1)=DDAWX
        XARY(2)=DDAWY
      END IF
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
      UGELV=0
      UGENM='        '
      UGEIX=0
  301 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  401 CALL UGRERR(3,'UGINFO  ',12)
      GO TO 301
C
      END

