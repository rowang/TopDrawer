      SUBROUTINE UGXGWS(FLAG,ARAY)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *       DUMMY INPUT/OUTPUT MODULE FOR THE GENERIC WORKSTATION       *
C *                                                                   *
C *  THIS SUBROUTINE IS A DUMMY INPUT/OUTPUT MODULE FOR THE GENERIC   *
C *  WORKSTATION WHEN IT IS USED AS A PSEUDO-DEVICE.  IT WILL BE      *
C *  INCORPORATED INTO THE LOAD MODULE IF THE USER DOES NOT INCLUDE   *
C *  AN ACTUAL SUBROUTINE.                                            *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGXGWS(FLAG,ARAY)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  A FLAG INDICATING THE OPERATION TO BE PERFORMED (0       *
C *          MEANS WRITE AND 1 MEANS READ).                           *
C *    ARAY  THE INPUT/OUTPUT ARRAY.                                  *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      INTEGER*2     ARAY(*)
C
C  TERMINATE EXECUTION IMMEDIATELY.
      CALL UGZ001
      STOP
C
      END

