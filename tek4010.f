      SUBROUTINE UGTX01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  DEVICE-DEPENDENT MODULE FOR THE                  *
C *              TEKTRONIX 4010 SERIES GRAPHIC TERMINALS              *
C *                   IN A FULLY INTERACTIVE MODE                     *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A FULLY         *
C *  INTERACTIVE GRAPHIC DISPLAY DEVICE.                              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxtin.for'
C
      INTEGER*4     INST(7)
      INTEGER*4     EXST(16)
      CHARACTER*64  EXCH
      EQUIVALENCE   (EXCH,EXST(1))
C
      CHARACTER*3   CLER
      CHARACTER*1   CLRX(3)
      EQUIVALENCE   (CLER,CLRX(1))
      CHARACTER*2   CRSH
      CHARACTER*1   CRSX(2)
      EQUIVALENCE   (CRSH,CRSX(1))
      CHARACTER*1   BELL
      CHARACTER*1   TERM
C
      INTEGER       INT1
C
      DATA          INST/1,4,7,1,64,'CHAN','NEL '/
c      DATA          CLRX/Z1D,Z1B,Z0C/
c      DATA          CRSX/Z1B,Z1A/
c      DATA          BELL/Z07/
c      DATA          TERM/Z1F/
      DATA          CLRX/29,27,12/
      DATA          CRSX/27,26/
      DATA          BELL/7/
      DATA          TERM/31/
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.14)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551,
     X       601,651,701,751),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXCH='TT'
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='TEK4010 '
      DDAIL=3
      DDAIC(1)=1
      DDAIC(5)=1
      DDABD(1,1)=0
      DDABD(1,2)=1023
      DDABD(2,1)=0
      DDABD(2,2)=779
      DDABX=0.0195
      DDABY=0.0195
      DDXID='DDA/TX00'
      DDXBN=0
      CALL UGTX07(EXCH,DDXIO,INT1)
      IF (INT1.NE.0) GO TO 902
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 CALL UGTX08(DDXIO)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 IF (DDIN(2).NE.0) GO TO 902
      DDXBF(1:3)=CLER
      DDXBN=3
      CALL UGTX04(0)
      CALL UGTX06(80)
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 DDXBN=0
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
  351 CALL UGTX04(1)
      GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
  451 IF (DDIN(3).LT.3) THEN
        DDXIL=0
      ELSE
        DDXIL=1
      END IF
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (461,471,481),INT1
  461 GO TO 901
  471 IF (DDIN(7).NE.1) GO TO 902
      GO TO 901
  481 IF ((DDIN(8).GE.5).AND.(DDIN(8).LE.355)) GO TO 902
      IF (DDIN(9).NE.2) THEN
        IF (DDIN(7).LT.11) GO TO 902
        IF (DDIN(7).GT.17) GO TO 902
      END IF
      DDEX(2)=-5
      DDEX(3)=-7
      DDEX(4)=14
      DDEX(5)=0
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (511,521,531),INT1
  511 CALL UGTX02(DDIN(3),DDIN(4),0)
      IF (DDXIL.EQ.0) THEN
        CALL UGTX02(DDIN(3),DDIN(4),1)
      ELSE
        CALL UGTX02(DDIN(3),DDIN(4)+1,1)
        IF (DDIN(3).EQ.1023) GO TO 901
        CALL UGTX02(DDIN(3)+1,DDIN(4)+1,1)
        CALL UGTX02(DDIN(3)+1,DDIN(4),1)
      END IF
      GO TO 901
  521 CALL UGTX02(DDIN(3),DDIN(4),DDIN(5))
      GO TO 901
  531 CALL UGTX03(DDIN(3),DDIN(4),DDST)
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 IF (DDIN(2).NE.1) GO TO 902
      DO 552 INT1=1,16
        DDXBF(INT1:INT1)=BELL
  552 CONTINUE
      DDXBN=16
      CALL UGTX04(0)
      CALL UGTX06(20)
      GO TO 901
C
C  OPERATION 11: MODIFY THE STATUS OF A CONTROL.
  601 IF (DDIN(2).NE.1) GO TO 902
      DDAKX=MIN(DDABD(1,2),MAX(0,DDAKX-5))
      DDAKY=MIN(DDABD(2,2),MAX(0,DDAKY-7))
      GO TO 901
C
C  OPERATION 12: ENABLE OR DISABLE A CONTROL.
  651 IF ((DDIN(3).NE.1).AND.(DDIN(3).NE.5)) GO TO 902
      GO TO 901
C
C  OPERATION 13: OBTAIN AN EVENT FROM THE GRAPHIC DEVICE.
  701 DDXBN=0
      CALL UGTX02(DDAKX,DDAKY,0)
      DDXBF(DDXBN+1:DDXBN+1)=TERM
      DDXBN=DDXBN+1
      CALL UGTX05(0,DDXBF(1:DDXBN))
      CALL UGTX10(DDXIO,DDIN(2),DDXBF,DDXBN,INT1)
      IF (INT1.EQ.-1) GO TO 901
      IF (DDABC(1).EQ.0) GO TO 701
      INT1=MIN(DDAZ2,INT1)
      DDEX(1)=1
      DDEX(2)=INT1
      IF (INT1.GT.0) THEN
        DDST(1:INT1)=DDXBF(1:INT1)
        IF (DDAKF.EQ.0) CALL UGTX05(2,DDST(1:INT1))
      END IF
      GO TO 903
C
C  OPERATION 14: SAMPLE AN INTERACTIVE CONTROL.
  751 IF (DDIN(2).NE.5) GO TO 902
  752 DDXBF(1:2)=CRSH
      CALL UGTX05(0,DDXBF(1:2))
      CALL UGTX10(DDXIO,-1,DDXBF,2,INT1)
      IF (INT1.NE.5) GO TO 752
      DDEX(1)=5
      CALL UGTX05(1,DDXBF(1:5))
      DDEX(2)=32*MOD(ICHAR(DDXBF(2:2)),32)+MOD(ICHAR(DDXBF(3:3)),32)
      DDEX(3)=32*MOD(ICHAR(DDXBF(4:4)),32)+MOD(ICHAR(DDXBF(5:5)),32)
      GO TO 903
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END
      SUBROUTINE UGTX02(XCRD,YCRD,BBIT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO GENERATE ORDERS TO MOVE THE BEAM TO   *
C *  A NEW POSITION.                                                  *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX02(XCRD,YCRD,BBIT)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE OF THE NEW POSITION.                    *
C *    YCRD  THE Y COORDINATE OF THE NEW POSITION.                    *
C *    BBIT  THE BLANKING BIT.                                        *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD,BBIT
C
      INCLUDE        'include/ugddxtin.for'
C
      INTEGER       MYHI,MYLO,MXHI,MXLO
      CHARACTER*1   BLKO
      INTEGER       CREQ
C
c      DATA          MYHI/Z00000020/
c      DATA          MYLO/Z00000060/
c      DATA          MXHI/Z00000020/
c      DATA          MXLO/Z00000040/
c      DATA          BLKO/Z1D/
      DATA          MYHI/32/
      DATA          MYLO/96/
      DATA          MXHI/32/
      DATA          MXLO/64/
      DATA          BLKO/29/
C
C  COMPUTE REQUIREMENTS AND WRITE OUT CURRENT RECORD IF NECESSARY.
      IF (BBIT.EQ.0) THEN
        CREQ=10
      ELSE
        CREQ=5
      END IF
      IF (CREQ.GT.(DDXZ1-DDXBN)) THEN
        CALL UGTX04(1)
        IF (BBIT.NE.0) THEN
          DDXBF(DDXBN+1:DDXBN+1)=BLKO
          DDXBF(DDXBN+2:DDXBN+5)=DDXOP
          DDXBN=DDXBN+5
        END IF
      END IF
C
C  CREATE THE NEW POINT AND ADD IT TO THE CURRENT RECORD.
      DDXOP(1:1)=CHAR(MOD(YCRD/32,32)+MYHI)
      DDXOP(2:2)=CHAR(MOD(YCRD   ,32)+MYLO)
      DDXOP(3:3)=CHAR(MOD(XCRD/32,32)+MXHI)
      DDXOP(4:4)=CHAR(MOD(XCRD   ,32)+MXLO)
      IF (BBIT.EQ.0) THEN
        DDXBF(DDXBN+1:DDXBN+1)=BLKO
        DDXBN=DDXBN+1
      END IF
      DDXBF(DDXBN+1:DDXBN+4)=DDXOP
      DDXBN=DDXBN+4
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX03(XCRD,YCRD,TEXT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO GENERATE ORDERS TO DISPLAY            *
C *  CHARACTERS AT A SPECIFIC POSITION.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX03(XCRD,YCRD,TEXT)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE OF THE FIRST CHARACTER.                 *
C *    YCRD  THE Y COORDINATE OF THE FIRST CHARACTER.                 *
C *    TEXT  THE CHARACTER STRING.                                    *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD
      CHARACTER*(*) TEXT
C
      INCLUDE        'include/ugddxtin.for'
C
      INTEGER       NTXT
      CHARACTER*80  ITXT
      CHARACTER*1   ECMO
      INTEGER       CRDX
      INTEGER       INSI,INSJ,INSN
C
c      DATA          ECMO/Z1F/
      DATA          ECMO/31/
C
C  TRANSLATE THE CHARACTERS AND INITIALIZE THE PROGRAM.
      NTXT=LEN(TEXT)
      ITXT(1:NTXT)=TEXT
      CALL UGTX05(1,ITXT(1:NTXT))
      CRDX=XCRD
      INSI=1
      INSN=NTXT
C
C  WRITE OUT THE CURRENT RECORD IF NECESSARY.
      IF (11.GT.(DDXZ1-DDXBN)) CALL UGTX04(1)
C
C  INSERT THE TEXT INTO THE RECORD.
  101 CALL UGTX02(CRDX,YCRD,0)
      DDXBF(DDXBN+1:DDXBN+1)=ECMO
      INSJ=MIN(DDXZ1-DDXBN-2,INSN)
      DDXBF(DDXBN+2:DDXBN+INSJ+1)=ITXT(INSI:INSI+INSJ-1)
      DDXBN=DDXBN+INSJ+1
      IF (INSJ.NE.INSN) THEN
        CRDX=CRDX+14*INSJ
        INSI=INSI+INSJ
        INSN=INSN-INSJ
        CALL UGTX04(1)
        GO TO 101
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX04(FLAG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO COMPLETE THE CURRENT RECORD AND       *
C *  WRITE IT OUT.  A TERMINAL BYTE MAY OPTIONALLY BE ADDED TO THE    *
C *  RECORD.                                                          *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX04(FLAG)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    FLAG  TERMINAL BYTE FLAG (0 MEANS DO NOT ADD TERMINAL BYTE, 1  *
C *          MEANS ADD TERMINAL BYTE).                                *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
C
      INCLUDE        'include/ugddxtin.for'
C
      CHARACTER*1   TERM
C
c      DATA          TERM/Z1D/
      DATA          TERM/29/
C
C  PROCESS THE RECORD AND WRITE IT OUT.
      IF (FLAG.NE.0) THEN
        DDXBF(DDXBN+1:DDXBN+1)=TERM
        DDXBN=DDXBN+1
      END IF
      CALL UGTX05(0,DDXBF(1:DDXBN))
      CALL UGTX09(DDXIO,DDXBF,DDXBN)
      DDXBN=0
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX05(FLAG,STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TRANSLATE CHARACTER STRINGS FROM THE  *
C *  TERMINAL CHARACTER SET TO THE COMPUTER CHARACTER SET, FROM THE   *
C *  COMPUTER CHARACTER SET TO THE TERMINAL CHARACTER SET, OR TO      *
C *  FORCE UPPER CASE LETTERS IN THE COMPUTER CHARACTER SET.          *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX05(FLAG,STRG)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  TRANSLATE FLAG (0 MEANS TERMINAL TO COMPUTER, 1 MEANS    *
C *          COMPUTER TO TERMINAL, 2 MEANS FORCE UPPER CASE).         *
C *    STRG  THE CHARACTER STRING TO BE TRANSLATED.                   *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      CHARACTER*(*) STRG
C
      INTEGER       INT1,INT2
      CHARACTER*1   CHR1
C
C  TRANSLATE THE CHARACTER STRING AS NEEDED.
      IF (FLAG.EQ.1) THEN
        DO 101 INT1=1,LEN(STRG)
          CHR1=STRG(INT1:INT1)
          IF ((CHR1.LT.' ').OR.(CHR1.GT.'~')) STRG(INT1:INT1)='@'
  101   CONTINUE
      ELSE IF (FLAG.EQ.2) THEN
        DO 102 INT1=1,LEN(STRG)
          INT2=ICHAR(STRG(INT1:INT1))
          IF ((INT2.GE.ICHAR('a')).AND.(INT2.LE.ICHAR('z')))
     X      STRG(INT1:INT1)=CHAR(INT2-ICHAR('a')+ICHAR('A'))
  102   CONTINUE
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX06(TIME)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO PUT THE PROGRAM INTO THE WAIT STATE   *
C *  FOR A SPECIFIED LENGTH OF TIME.                                  *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX06(TIME)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    TIME  THE DELAY TIME IN HUNDREDTHS OF A SECOND.                *
C *                                                                   *
C *********************************************************************
C
      INTEGER       TIME
C
      INTEGER       SYS$BINTIM,SYS$SCHDWK
C
      CHARACTER*13  CTIM
      INTEGER*4     XTIM(2)
C
      INTEGER       INT1
C
      DATA          CTIM/'0 00:00:XX.XX'/
C
C  SUSPEND EXECUTION FOR THE INDICATED INTERVAL.
      CALL UGCNVF(REAL(TIME)/100.0,2,CTIM(9:13),INT1)
      IF (CTIM(9:9).EQ.' ') CTIM(9:9)='0'
      INT1=SYS$BINTIM(CTIM,XTIM)
      IF (.NOT.INT1) CALL UGZ001
      INT1=SYS$SCHDWK(,,XTIM,)
      IF (.NOT.INT1) CALL UGZ001
      CALL SYS$HIBER()
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX07(NAME,PIOD,FLAG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THESE SUBROUTINES ARE THE ACTUAL INPUT/OUTPUT MODULES.  UGTX07   *
C *  IS THE OPEN MODULE, UGTX08 IS THE CLOSE MODULE, UGTX09 IS THE    *
C *  WRITE MODULE, AND UGTX10 IS THE READ WITH PROMPT MODULE.         *
C *                                                                   *
C *  THE CALLING SEQUENCES ARE:                                       *
C *    CALL UGTX07(NAME,PIOD,FLAG)                                    *
C *    CALL UGTX08(PIOD)                                              *
C *    CALL UGTX09(PIOD,STRG,WCNT)                                    *
C *    CALL UGTX10(PIOD,TIME,STRG,WCNT,RCNT)                          *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCES ARE:                     *
C *    NAME  THE NAME OF THE INPUT/OUTPUT CHANNEL.  THE CHARACTER     *
C *          STRING SHOULD BE 64 CHARACTERS LONG.                     *
C *    PIOD  THE CHANNEL IDENTIFICATION.  THIS ITEM IS GENERATED BY   *
C *          THE OPEN FUNCTION AND MUST BE SUPPLIED TO THE OTHER      *
C *          FUNCTIONS.                                               *
C *    FLAG  A SUCCESS FLAG (0 MEANS SUCCESS, 1 MEANS ERROR).         *
C *    STRG  THE INPUT/OUTPUT CHARACTER STRING.                       *
C *    WCNT  THE WRITE COUNT FOR THE CHARACTERS IN STRG.              *
C *    TIME  THE DELAY TIME IN HUNDREDTHS OF A SECOND.  A VALUE OF    *
C *          -1 INDICATES NO TIME-OUT IS REQUIRED.                    *
C *    RCNT  THE READ COUNT FOR THE CHARACTERS IN STRG.  A -1 VALUE   *
C *          INDICATES A TIME-OUT HAS OCCURRED.                       *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*64  NAME
      INTEGER       PIOD,FLAG
      CHARACTER*(*) STRG
      INTEGER       TIME,WCNT,RCNT
C
      INCLUDE        'include/ugddxtin.for'
C
      INCLUDE        'include/ugioparm.for'
C
      EXTERNAL      UGTX11,UGTX12
      INTEGER       SYS$ASSIGN,SYS$DASSGN,
     X              SYS$BINTIM,SYS$SETIMR,SYS$CANTIM,
     X              SYS$QIO,SYS$QIOW,SYS$CANCEL
C
      CHARACTER*13  CTIM
      INTEGER*4     XTIM(2)
      INTEGER*2     IOSB(4)
C
      INTEGER       INT1,INT2
C
      DATA          CTIM/'0 00:XX:XX.XX'/
C
C  PERFORM THE OPEN FUNCTION.
      DO 101 INT1=1,64
        IF (NAME(INT1:INT1).EQ.' ') THEN
          INT2=INT1-1
          GO TO 102
        END IF
  101 CONTINUE
      INT2=64
  102 INT1=SYS$ASSIGN(NAME(1:INT2),PIOD,,)
      IF (.NOT.INT1) THEN
        FLAG=1
      ELSE
        FLAG=0
      END IF
      GO TO 301
C
C  PERFORM THE CLOSE FUNCTION.
      ENTRY UGTX08(PIOD)
      INT1=SYS$DASSGN(%VAL(PIOD))
      IF (.NOT.INT1) CALL UGZ001
      GO TO 301
C
C  PERFORM THE WRITE FUNCTION.
      ENTRY UGTX09(PIOD,STRG,WCNT)
      INT1=SYS$QIOW(,%VAL(PIOD),
     X  %VAL(IO$_WRITEVBLK+IO$M_NOFORMAT),,,,
     X  %REF(STRG),%VAL(WCNT),,%VAL('20'X),,)
      IF (.NOT.INT1) CALL UGZ001
      GO TO 301
C
C  PERFORM THE READ FUNCTION.
      ENTRY UGTX10(PIOD,TIME,STRG,WCNT,RCNT)
      IF (TIME.GE.0) THEN
        INT1=MOD(TIME,6000)
        CALL UGCNVF(REAL(INT1)/100.0,2,CTIM(9:13),INT2)
        IF (CTIM(9:9).EQ.' ') CTIM(9:9)='0'
        INT1=(TIME-INT1)/6000
        CALL UGCNVF(REAL(INT1),0,CTIM(6:7),INT2)
        IF (CTIM(6:6).EQ.' ') CTIM(6:6)='0'
        INT1=SYS$BINTIM(CTIM,XTIM)
        IF (.NOT.INT1) CALL UGZ001
        DDXHT=0
        INT1=SYS$SETIMR(,XTIM,UGTX11,%VAL(99))
        IF (.NOT.INT1) CALL UGZ001
      END IF
      INT1=SYS$QIOW(,%VAL(PIOD),
     X  %VAL(IO$_WRITEVBLK+IO$M_NOFORMAT),,,,
     X  %REF(STRG),%VAL(WCNT),,%VAL('24'X),,)
      IF (.NOT.INT1) CALL UGZ001
      DDXHQ=0
      INT1=SYS$QIO(,%VAL(PIOD),
     X  %VAL(IO$_READVBLK),IOSB,UGTX12,,
     X  %REF(STRG),%VAL(LEN(STRG)),,,,)
      IF (.NOT.INT1) CALL UGZ001
  201 CALL SYS$HIBER()
      IF (DDXHQ.EQ.1) THEN
        RCNT=IOSB(2)
        IF (TIME.GE.0) THEN
          DDXHT=-1
          INT1=SYS$CANTIM(%VAL(99),)
          IF (.NOT.INT1) CALL UGZ001
        END IF
        GO TO 301
      ELSE IF (DDXHT.EQ.1) THEN
        DDXHQ=-1
        INT1=SYS$CANCEL(%VAL(PIOD))
        IF (.NOT.INT1) CALL UGZ001
        RCNT=-1
        GO TO 301
      END IF
      GO TO 201
C
C  RETURN TO CALLER.
  301 RETURN
C
      END
      SUBROUTINE UGTX11
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS ENTERED WHENEVER THE TIME INTERVAL            *
C *  ESTABLISHED IN UGTX10 EXPIRES.                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX11                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE        'include/ugddxtin.for'
C
      IF (DDXHT.EQ.0) THEN
        DDXHT=1
        CALL SYS$WAKE(,)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTX12
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS ENTERED WHENEVER KEYBOARD INPUT IS READY TO   *
C *  BE PROCESSED IN SUBROUTINE UGTX10.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTX12                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE        'include/ugddxtin.for'
C
      IF (DDXHQ.EQ.0) THEN
        DDXHQ=1
        CALL SYS$WAKE(,)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END

